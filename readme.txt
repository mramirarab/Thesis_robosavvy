1- Installing Kinect driver
The first step in installing the python wrapper is to clone the libfreenect repository from tis link:
https://github.com/OpenKinect/libfreenect
There are some prerequisites needed for freenect library to install.

$ sudo apt-get install git cmake build-essential libusb-1.0-0-dev
$ sudo apt-get install freeglut3-dev libxmu-dev libxi-dev

Now the freenect library can be installed and built.

$ Cd ~
$ git clone https://github.com/OpenKinect/libfreenect
$ cd libfreenect
$ mkdir build
$ cd build
$ cmake .. -DCMAKE_BUILD_PYTHON=ON
$ sudo make install

It is very important that at this point you get no error otherwise, google the error and make sure the build is successful.
Ubuntu normally comes with 2 versions of Python. At the time of writing this manual, Python 2.7 and 3.5 and preinstalled on Ubuntu 16.04. 
It is possible to use eaither of them for this project; however, we have encountered some errors when using python 3 and raspy. 
Thus, we strongly encourage the reader to use Python 2 for this project. 
It is important that you know which version of Python is called by default for you in your bash terminal by is running:

 $ python –version
 
If the default Python is Python 2, in order to use Python 3 to run a Python script, you should specify which Python version should be used to interpret it:

$ python <location of the script>/<script>.py 

This will call the default Python i.e. the one that is shown by running python –version.

$python2 <location of the script>/</<script>.py

This will force bash to use Python 2 to run the script.

$ python3 <location of the script>/</<script>.py 

This will force bash to use Python 3 to run the script.
If the result shows python2. <version> you need to get pip for python2 as follows:

$ sudo apt-get install python-pip 

Else for Python3 use:

$ sudo apt-get install python3-pip

Once pip is installed, the prerequisites for the Python wrapper need to be installed by the following commands.
Please note that depending on the python version, pip3 or pip2 should be used.

$ sudo Pip2 install numpy or sudo pip3 install numpy
$ sudo Pip2 install cython or sudo Pip3 install cython
$ sudo Pip2 install opencv-python or sudo pip3 install opencv-python
$ sudo apt-get install python-dev

For the next step, the bash terminal should point to the python wrapper folder.

$ cd ~/libfreenect-master/wrappers/python

Install the Python wrapper:

$ sudo python setup.py install or sudo python3 setup.py

If you encounter any trouble while installing Python wrapper, 
it is a good idea to look at the README file at ~/libfreenect-master/wrappers/python/README
For the 2D facial recognition, the face_recognition package should be installed:

$ sudo pip2 install face_recognition or sudo pip3 install face_recognition


2-Setting up the Raspberry pi 3B
In this repository, the backup image of the raspberry pi 3 is provided. 
In order to write the image into a micro SD card, you need to download the program called etcher from  https://etcher.io/. 
Once  etcher is installed, you need a micro SD card that is at least 8GB. Using etcher is simple, just select the unziped 
raspberry pi image, select the micro SD card and click on flash. Once it is flashed onto your card, you can insert it 
into raspberry pi. Please note that the capacity of micro SD cards are not exactly the same. That is why, you might 
encounter an error that the size of your micro SD card is small although you have an 8GB micro SD card. Thus, it is 
recommended to use a micro SD card that is larger than 8GB.
In this image, all the dependencied needed for runnning this project is available i.e. ROS is installed, all the Robosavvy 
communication programs are installed. The Python scripts for manually driving the robot and using the ultrasonic sensors for 
stopping the robot from crashing into objects is available in the home directory. The raspberry pi is configured to run an SSH 
server. Thus, it is possible to connecto to the raspberry pi via ssh thorough the command below:

$ ssh ubuntu@<ip address of the raspberry pi>

In is worth noting that the ip address of the raspberry pi shpuld be configured in your router to be static at 192.168.0.31
to be able to communicate with the robot. The password for the raspberry pi is available below. Please mind the capital U :

Passwprd : piUbuntu

This will connect your terminal remotely to the raspberry pi. To start the robot, ROS should be running using the command below:

$ roscore

It is worth noting that it is possible to run any program in background by utilizing ‘&’ after the name of the program:

$ roscore &

This is especially important since roscore should be running all the time in the background. Hence, the only two solutions 
are either using and ‘&’ after the program name or openning new ssh session in a new terminal.
Once ROS is running, you can turn the robot on. After hearing 3 beeps, the robot is ready to be connected via ros serial:

$ rosrun rosserial_python serial_node.py _port:=tcp

If the Ip address of the raspberry pi is configured correctly, you should be able to see that the communication is made 
successful. Otherwise rosserial will wait for the connection as shown below:

 [INFO] [1528197603.443610]: ROS Serial Python Node
Fork_server is:  False
[INFO] [1528197603.490057]: Waiting for socket connections on port 11411
waiting for socket connection

If the connection is successful, you will be able to see the messages below:

[INFO] [1528197604.371964]: Established a socket connection from 192.168.0.7 on port 49305
[INFO] [1528197604.374478]: calling startSerialClient
[INFO] [1528197612.655410]: Note: publish buffer size is 1024 bytes
[INFO] [1528197612.658359]: Setup publisher on /tf [tf/tfMessage]
[INFO] [1528197612.714014]: Setup publisher on odom [nav_msgs/Odometry]
[INFO] [1528197612.760319]: Setup publisher on state [gd_msgs/FullStateVelT]
[INFO] [1528197612.775132]: Setup publisher on battery_platform [std_msgs/Float32]
[INFO] [1528197612.789828]: Setup publisher on charging_platform [std_msgs/Bool]
[INFO] [1528197612.819751]: Setup service server on reset_odom [std_srvs/Empty]
[INFO] [1528197612.844585]: Setup service server on set_input [gd_msgs/SetInput]
[INFO] [1528197612.859293]: Setup service server on init_balance [std_srvs/Empty]
[INFO] [1528197612.873962]: Setup service server on platform_off [std_srvs/Empty]
[INFO] [1528197612.896707]: Note: subscribe buffer size is 512 bytes
[INFO] [1528197612.899317]: Setup subscriber on cmd_vel [geometry_msgs/Twist]
[INFO] [1528197612.921600]: Setup subscriber on cmd_height [std_msgs/Float32]
[INFO] [1528197612.942735]: Setup subscriber on calib_center [std_msgs/Float32]
[INFO] [1528197612.965076]: Setup subscriber on matrixA [std_msgs/Float32MultiArray]
[INFO] [1528197612.987972]: Setup subscriber on matrixB [std_msgs/Float32MultiArray]
[INFO] [1528197613.009746]: Setup subscriber on matrixK [std_msgs/Float32MultiArray]
[INFO] [1528197613.032315]: Setup subscriber on matrixL [std_msgs/Float32MultiArray]
[INFO] [1528197613.055557]: Setup subscriber on park [std_msgs/Float32]
[INFO] [1528197613.069443]: Finished loading parameters.

In order to self balance the robot, a matrix file should be generate based on the possition of the center of mass and the
height of the center of mass:

$ cd ~/gd_Calculator
$ ./gd_Calculator <weight [kg]> <distance payloadCM-base [m]>  ~/ros_ws/src/gd_msgs/scripts/

Once the matrix file is generated, there will be 2 file called info.txt and matrix.txt in the path /ros_ws/src/gd_msgs/scripts/.
In order to run the self balancing script, you need to run the following commands:

$ cd ~/ros_ws/src/gd_msgs/scripts/
$ python ~/ros_ws/src/gd_msgs/scripts/startup_parameters.py <value> 0

The value should be in the range of (-0.1 ,0.1) and you should find the appropriate value for that though trial and error by using
the calibration command mentioned below; however, to start the robot for the first time, you should use an arbitrary value to be able to find the calibrated value later on.

$ rostopic pub -1 /calib_center std_msgs/Float32 "data: <value>"

In order to move the robot manually, there is a python script provided in the home directory of the raspberry pi. With this script,
you can use your keys on the keyboard to control the robot. In the table below, you can see what keys can be used for this purpose:

Up arrow :	Move the robot forwards
Down arrow :	Move the robot backwards
Right arrow :	Turn the robot counter clockwise
Left arrow :	Turn the robot clockwise
e :	Exit the program
p :	Park the robot
It is worth mentioning that by pressing the same arrow key repeatadly, the robot will speed up in the coresponding direction and the
current speed will be printed in the terminal. For running the manual robot control use the command:

$ python ~/robotControl.py
