// Generated by gencpp from file gd_msgs/FullStateSim.msg
// DO NOT EDIT!


#ifndef GD_MSGS_MESSAGE_FULLSTATESIM_H
#define GD_MSGS_MESSAGE_FULLSTATESIM_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <gd_msgs/State.h>
#include <gd_msgs/State.h>
#include <gd_msgs/State.h>

namespace gd_msgs
{
template <class ContainerAllocator>
struct FullStateSim_
{
  typedef FullStateSim_<ContainerAllocator> Type;

  FullStateSim_()
    : wip_state()
    , desired_state()
    , fdbk_state()
    , u_eq_left(0.0)
    , u_eq_right(0.0)
    , phi_eq(0.0)
    , u_left(0.0)
    , u_right(0.0)
    , w_left(0.0)
    , w_right(0.0)
    , roll(0.0)
    , pitch(0.0)
    , ramp_psi(0.0)
    , ramp_delta(0.0)  {
    }
  FullStateSim_(const ContainerAllocator& _alloc)
    : wip_state(_alloc)
    , desired_state(_alloc)
    , fdbk_state(_alloc)
    , u_eq_left(0.0)
    , u_eq_right(0.0)
    , phi_eq(0.0)
    , u_left(0.0)
    , u_right(0.0)
    , w_left(0.0)
    , w_right(0.0)
    , roll(0.0)
    , pitch(0.0)
    , ramp_psi(0.0)
    , ramp_delta(0.0)  {
  (void)_alloc;
    }



   typedef  ::gd_msgs::State_<ContainerAllocator>  _wip_state_type;
  _wip_state_type wip_state;

   typedef  ::gd_msgs::State_<ContainerAllocator>  _desired_state_type;
  _desired_state_type desired_state;

   typedef  ::gd_msgs::State_<ContainerAllocator>  _fdbk_state_type;
  _fdbk_state_type fdbk_state;

   typedef float _u_eq_left_type;
  _u_eq_left_type u_eq_left;

   typedef float _u_eq_right_type;
  _u_eq_right_type u_eq_right;

   typedef float _phi_eq_type;
  _phi_eq_type phi_eq;

   typedef float _u_left_type;
  _u_left_type u_left;

   typedef float _u_right_type;
  _u_right_type u_right;

   typedef float _w_left_type;
  _w_left_type w_left;

   typedef float _w_right_type;
  _w_right_type w_right;

   typedef float _roll_type;
  _roll_type roll;

   typedef float _pitch_type;
  _pitch_type pitch;

   typedef float _ramp_psi_type;
  _ramp_psi_type ramp_psi;

   typedef float _ramp_delta_type;
  _ramp_delta_type ramp_delta;





  typedef boost::shared_ptr< ::gd_msgs::FullStateSim_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::gd_msgs::FullStateSim_<ContainerAllocator> const> ConstPtr;

}; // struct FullStateSim_

typedef ::gd_msgs::FullStateSim_<std::allocator<void> > FullStateSim;

typedef boost::shared_ptr< ::gd_msgs::FullStateSim > FullStateSimPtr;
typedef boost::shared_ptr< ::gd_msgs::FullStateSim const> FullStateSimConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::gd_msgs::FullStateSim_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::gd_msgs::FullStateSim_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace gd_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'gd_msgs': ['/home/gil/catkin_ws/src/gd_msgs/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::gd_msgs::FullStateSim_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::gd_msgs::FullStateSim_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::gd_msgs::FullStateSim_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::gd_msgs::FullStateSim_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::gd_msgs::FullStateSim_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::gd_msgs::FullStateSim_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::gd_msgs::FullStateSim_<ContainerAllocator> >
{
  static const char* value()
  {
    return "8b31cdf4b9e337ab7b2e4b6fe00facc0";
  }

  static const char* value(const ::gd_msgs::FullStateSim_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x8b31cdf4b9e337abULL;
  static const uint64_t static_value2 = 0x7b2e4b6fe00facc0ULL;
};

template<class ContainerAllocator>
struct DataType< ::gd_msgs::FullStateSim_<ContainerAllocator> >
{
  static const char* value()
  {
    return "gd_msgs/FullStateSim";
  }

  static const char* value(const ::gd_msgs::FullStateSim_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::gd_msgs::FullStateSim_<ContainerAllocator> >
{
  static const char* value()
  {
    return "# WIP State\n\
State wip_state\n\
State desired_state\n\
State fdbk_state\n\
\n\
# Equilibrium condition:\n\
float32 u_eq_left\n\
float32 u_eq_right\n\
float32 phi_eq\n\
\n\
# Current actuation:\n\
float32 u_left\n\
float32 u_right\n\
\n\
# Current wheel velocity:\n\
float32 w_left\n\
float32 w_right\n\
\n\
# IMU sensed:\n\
float32 roll\n\
float32 pitch\n\
\n\
# Sensed floor:\n\
float32 ramp_psi\n\
float32 ramp_delta\n\
\n\
\n\
================================================================================\n\
MSG: gd_msgs/State\n\
# State space vector for balance system\n\
float32 phi\n\
float32 dx\n\
float32 dpsi\n\
float32 dphi\n\
";
  }

  static const char* value(const ::gd_msgs::FullStateSim_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::gd_msgs::FullStateSim_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.wip_state);
      stream.next(m.desired_state);
      stream.next(m.fdbk_state);
      stream.next(m.u_eq_left);
      stream.next(m.u_eq_right);
      stream.next(m.phi_eq);
      stream.next(m.u_left);
      stream.next(m.u_right);
      stream.next(m.w_left);
      stream.next(m.w_right);
      stream.next(m.roll);
      stream.next(m.pitch);
      stream.next(m.ramp_psi);
      stream.next(m.ramp_delta);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct FullStateSim_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::gd_msgs::FullStateSim_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::gd_msgs::FullStateSim_<ContainerAllocator>& v)
  {
    s << indent << "wip_state: ";
    s << std::endl;
    Printer< ::gd_msgs::State_<ContainerAllocator> >::stream(s, indent + "  ", v.wip_state);
    s << indent << "desired_state: ";
    s << std::endl;
    Printer< ::gd_msgs::State_<ContainerAllocator> >::stream(s, indent + "  ", v.desired_state);
    s << indent << "fdbk_state: ";
    s << std::endl;
    Printer< ::gd_msgs::State_<ContainerAllocator> >::stream(s, indent + "  ", v.fdbk_state);
    s << indent << "u_eq_left: ";
    Printer<float>::stream(s, indent + "  ", v.u_eq_left);
    s << indent << "u_eq_right: ";
    Printer<float>::stream(s, indent + "  ", v.u_eq_right);
    s << indent << "phi_eq: ";
    Printer<float>::stream(s, indent + "  ", v.phi_eq);
    s << indent << "u_left: ";
    Printer<float>::stream(s, indent + "  ", v.u_left);
    s << indent << "u_right: ";
    Printer<float>::stream(s, indent + "  ", v.u_right);
    s << indent << "w_left: ";
    Printer<float>::stream(s, indent + "  ", v.w_left);
    s << indent << "w_right: ";
    Printer<float>::stream(s, indent + "  ", v.w_right);
    s << indent << "roll: ";
    Printer<float>::stream(s, indent + "  ", v.roll);
    s << indent << "pitch: ";
    Printer<float>::stream(s, indent + "  ", v.pitch);
    s << indent << "ramp_psi: ";
    Printer<float>::stream(s, indent + "  ", v.ramp_psi);
    s << indent << "ramp_delta: ";
    Printer<float>::stream(s, indent + "  ", v.ramp_delta);
  }
};

} // namespace message_operations
} // namespace ros

#endif // GD_MSGS_MESSAGE_FULLSTATESIM_H
