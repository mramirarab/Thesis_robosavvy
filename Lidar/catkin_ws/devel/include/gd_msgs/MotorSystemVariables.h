// Generated by gencpp from file gd_msgs/MotorSystemVariables.msg
// DO NOT EDIT!


#ifndef GD_MSGS_MESSAGE_MOTORSYSTEMVARIABLES_H
#define GD_MSGS_MESSAGE_MOTORSYSTEMVARIABLES_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace gd_msgs
{
template <class ContainerAllocator>
struct MotorSystemVariables_
{
  typedef MotorSystemVariables_<ContainerAllocator> Type;

  MotorSystemVariables_()
    : position(0.0)
    , velocity(0.0)
    , k(0.0)
    , ki(0.0)
    , kd(0.0)  {
    }
  MotorSystemVariables_(const ContainerAllocator& _alloc)
    : position(0.0)
    , velocity(0.0)
    , k(0.0)
    , ki(0.0)
    , kd(0.0)  {
  (void)_alloc;
    }



   typedef float _position_type;
  _position_type position;

   typedef float _velocity_type;
  _velocity_type velocity;

   typedef float _k_type;
  _k_type k;

   typedef float _ki_type;
  _ki_type ki;

   typedef float _kd_type;
  _kd_type kd;





  typedef boost::shared_ptr< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> const> ConstPtr;

}; // struct MotorSystemVariables_

typedef ::gd_msgs::MotorSystemVariables_<std::allocator<void> > MotorSystemVariables;

typedef boost::shared_ptr< ::gd_msgs::MotorSystemVariables > MotorSystemVariablesPtr;
typedef boost::shared_ptr< ::gd_msgs::MotorSystemVariables const> MotorSystemVariablesConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::gd_msgs::MotorSystemVariables_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace gd_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': True, 'IsMessage': True, 'HasHeader': False}
// {'gd_msgs': ['/home/gil/catkin_ws/src/gd_msgs/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ad727b23c5a39de4b0eda6c33227bf59";
  }

  static const char* value(const ::gd_msgs::MotorSystemVariables_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xad727b23c5a39de4ULL;
  static const uint64_t static_value2 = 0xb0eda6c33227bf59ULL;
};

template<class ContainerAllocator>
struct DataType< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
{
  static const char* value()
  {
    return "gd_msgs/MotorSystemVariables";
  }

  static const char* value(const ::gd_msgs::MotorSystemVariables_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
{
  static const char* value()
  {
    return "float32 position\n\
float32 velocity\n\
float32 k\n\
float32 ki\n\
float32 kd\n\
";
  }

  static const char* value(const ::gd_msgs::MotorSystemVariables_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.position);
      stream.next(m.velocity);
      stream.next(m.k);
      stream.next(m.ki);
      stream.next(m.kd);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct MotorSystemVariables_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::gd_msgs::MotorSystemVariables_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::gd_msgs::MotorSystemVariables_<ContainerAllocator>& v)
  {
    s << indent << "position: ";
    Printer<float>::stream(s, indent + "  ", v.position);
    s << indent << "velocity: ";
    Printer<float>::stream(s, indent + "  ", v.velocity);
    s << indent << "k: ";
    Printer<float>::stream(s, indent + "  ", v.k);
    s << indent << "ki: ";
    Printer<float>::stream(s, indent + "  ", v.ki);
    s << indent << "kd: ";
    Printer<float>::stream(s, indent + "  ", v.kd);
  }
};

} // namespace message_operations
} // namespace ros

#endif // GD_MSGS_MESSAGE_MOTORSYSTEMVARIABLES_H
