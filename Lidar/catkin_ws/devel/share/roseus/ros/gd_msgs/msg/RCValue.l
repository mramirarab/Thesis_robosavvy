;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::RCValue)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'RCValue (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::RCVALUE")
  (make-package "GD_MSGS::RCVALUE"))

(in-package "ROS")
;;//! \htmlinclude RCValue.msg.html


(defclass gd_msgs::RCValue
  :super ros::object
  :slots (_connected _value ))

(defmethod gd_msgs::RCValue
  (:init
   (&key
    ((:connected __connected) nil)
    ((:value __value) 0)
    )
   (send-super :init)
   (setq _connected __connected)
   (setq _value (round __value))
   self)
  (:connected
   (&optional __connected)
   (if __connected (setq _connected __connected)) _connected)
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; bool _connected
    1
    ;; int16 _value
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _connected
       (if _connected (write-byte -1 s) (write-byte 0 s))
     ;; int16 _value
       (write-word _value s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _connected
     (setq _connected (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int16 _value
     (setq _value (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get gd_msgs::RCValue :md5sum-) "f8111cd0d17d8f3a681cc1d3196c2d07")
(setf (get gd_msgs::RCValue :datatype-) "gd_msgs/RCValue")
(setf (get gd_msgs::RCValue :definition-)
      "bool connected
int16 value

")



(provide :gd_msgs/RCValue "f8111cd0d17d8f3a681cc1d3196c2d07")


