;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::MotorSystemVariables)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'MotorSystemVariables (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::MOTORSYSTEMVARIABLES")
  (make-package "GD_MSGS::MOTORSYSTEMVARIABLES"))

(in-package "ROS")
;;//! \htmlinclude MotorSystemVariables.msg.html


(defclass gd_msgs::MotorSystemVariables
  :super ros::object
  :slots (_position _velocity _k _ki _kd ))

(defmethod gd_msgs::MotorSystemVariables
  (:init
   (&key
    ((:position __position) 0.0)
    ((:velocity __velocity) 0.0)
    ((:k __k) 0.0)
    ((:ki __ki) 0.0)
    ((:kd __kd) 0.0)
    )
   (send-super :init)
   (setq _position (float __position))
   (setq _velocity (float __velocity))
   (setq _k (float __k))
   (setq _ki (float __ki))
   (setq _kd (float __kd))
   self)
  (:position
   (&optional __position)
   (if __position (setq _position __position)) _position)
  (:velocity
   (&optional __velocity)
   (if __velocity (setq _velocity __velocity)) _velocity)
  (:k
   (&optional __k)
   (if __k (setq _k __k)) _k)
  (:ki
   (&optional __ki)
   (if __ki (setq _ki __ki)) _ki)
  (:kd
   (&optional __kd)
   (if __kd (setq _kd __kd)) _kd)
  (:serialization-length
   ()
   (+
    ;; float32 _position
    4
    ;; float32 _velocity
    4
    ;; float32 _k
    4
    ;; float32 _ki
    4
    ;; float32 _kd
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _position
       (sys::poke _position (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _velocity
       (sys::poke _velocity (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _k
       (sys::poke _k (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ki
       (sys::poke _ki (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _kd
       (sys::poke _kd (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _position
     (setq _position (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _velocity
     (setq _velocity (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _k
     (setq _k (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ki
     (setq _ki (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _kd
     (setq _kd (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gd_msgs::MotorSystemVariables :md5sum-) "ad727b23c5a39de4b0eda6c33227bf59")
(setf (get gd_msgs::MotorSystemVariables :datatype-) "gd_msgs/MotorSystemVariables")
(setf (get gd_msgs::MotorSystemVariables :definition-)
      "float32 position
float32 velocity
float32 k
float32 ki
float32 kd

")



(provide :gd_msgs/MotorSystemVariables "ad727b23c5a39de4b0eda6c33227bf59")


