;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::FullState)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'FullState (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::FULLSTATE")
  (make-package "GD_MSGS::FULLSTATE"))

(in-package "ROS")
;;//! \htmlinclude FullState.msg.html


(defclass gd_msgs::FullState
  :super ros::object
  :slots (_wip_state _desired_state _fdbk_state _u_left _u_right ))

(defmethod gd_msgs::FullState
  (:init
   (&key
    ((:wip_state __wip_state) (instance gd_msgs::State :init))
    ((:desired_state __desired_state) (instance gd_msgs::State :init))
    ((:fdbk_state __fdbk_state) (instance gd_msgs::State :init))
    ((:u_left __u_left) 0.0)
    ((:u_right __u_right) 0.0)
    )
   (send-super :init)
   (setq _wip_state __wip_state)
   (setq _desired_state __desired_state)
   (setq _fdbk_state __fdbk_state)
   (setq _u_left (float __u_left))
   (setq _u_right (float __u_right))
   self)
  (:wip_state
   (&rest __wip_state)
   (if (keywordp (car __wip_state))
       (send* _wip_state __wip_state)
     (progn
       (if __wip_state (setq _wip_state (car __wip_state)))
       _wip_state)))
  (:desired_state
   (&rest __desired_state)
   (if (keywordp (car __desired_state))
       (send* _desired_state __desired_state)
     (progn
       (if __desired_state (setq _desired_state (car __desired_state)))
       _desired_state)))
  (:fdbk_state
   (&rest __fdbk_state)
   (if (keywordp (car __fdbk_state))
       (send* _fdbk_state __fdbk_state)
     (progn
       (if __fdbk_state (setq _fdbk_state (car __fdbk_state)))
       _fdbk_state)))
  (:u_left
   (&optional __u_left)
   (if __u_left (setq _u_left __u_left)) _u_left)
  (:u_right
   (&optional __u_right)
   (if __u_right (setq _u_right __u_right)) _u_right)
  (:serialization-length
   ()
   (+
    ;; gd_msgs/State _wip_state
    (send _wip_state :serialization-length)
    ;; gd_msgs/State _desired_state
    (send _desired_state :serialization-length)
    ;; gd_msgs/State _fdbk_state
    (send _fdbk_state :serialization-length)
    ;; float32 _u_left
    4
    ;; float32 _u_right
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; gd_msgs/State _wip_state
       (send _wip_state :serialize s)
     ;; gd_msgs/State _desired_state
       (send _desired_state :serialize s)
     ;; gd_msgs/State _fdbk_state
       (send _fdbk_state :serialize s)
     ;; float32 _u_left
       (sys::poke _u_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _u_right
       (sys::poke _u_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; gd_msgs/State _wip_state
     (send _wip_state :deserialize buf ptr-) (incf ptr- (send _wip_state :serialization-length))
   ;; gd_msgs/State _desired_state
     (send _desired_state :deserialize buf ptr-) (incf ptr- (send _desired_state :serialization-length))
   ;; gd_msgs/State _fdbk_state
     (send _fdbk_state :deserialize buf ptr-) (incf ptr- (send _fdbk_state :serialization-length))
   ;; float32 _u_left
     (setq _u_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _u_right
     (setq _u_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gd_msgs::FullState :md5sum-) "c813ba5aa7cf30286a706a0a3e0582cb")
(setf (get gd_msgs::FullState :datatype-) "gd_msgs/FullState")
(setf (get gd_msgs::FullState :definition-)
      "# WIP State
State wip_state
State desired_state
State fdbk_state

# Current actuation:
float32 u_left
float32 u_right

================================================================================
MSG: gd_msgs/State
# State space vector for balance system
float32 phi
float32 dx
float32 dpsi
float32 dphi

")



(provide :gd_msgs/FullState "c813ba5aa7cf30286a706a0a3e0582cb")


