;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::BatteryRegs)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'BatteryRegs (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::BATTERYREGS")
  (make-package "GD_MSGS::BATTERYREGS"))

(in-package "ROS")
;;//! \htmlinclude BatteryRegs.msg.html


(defclass gd_msgs::BatteryRegs
  :super ros::object
  :slots (_connected _temperature _voltage _current _remaining_capacity _cycles_count ))

(defmethod gd_msgs::BatteryRegs
  (:init
   (&key
    ((:connected __connected) nil)
    ((:temperature __temperature) 0)
    ((:voltage __voltage) 0)
    ((:current __current) 0)
    ((:remaining_capacity __remaining_capacity) 0)
    ((:cycles_count __cycles_count) 0)
    )
   (send-super :init)
   (setq _connected __connected)
   (setq _temperature (round __temperature))
   (setq _voltage (round __voltage))
   (setq _current (round __current))
   (setq _remaining_capacity (round __remaining_capacity))
   (setq _cycles_count (round __cycles_count))
   self)
  (:connected
   (&optional __connected)
   (if __connected (setq _connected __connected)) _connected)
  (:temperature
   (&optional __temperature)
   (if __temperature (setq _temperature __temperature)) _temperature)
  (:voltage
   (&optional __voltage)
   (if __voltage (setq _voltage __voltage)) _voltage)
  (:current
   (&optional __current)
   (if __current (setq _current __current)) _current)
  (:remaining_capacity
   (&optional __remaining_capacity)
   (if __remaining_capacity (setq _remaining_capacity __remaining_capacity)) _remaining_capacity)
  (:cycles_count
   (&optional __cycles_count)
   (if __cycles_count (setq _cycles_count __cycles_count)) _cycles_count)
  (:serialization-length
   ()
   (+
    ;; bool _connected
    1
    ;; int16 _temperature
    2
    ;; int16 _voltage
    2
    ;; int16 _current
    2
    ;; int16 _remaining_capacity
    2
    ;; int16 _cycles_count
    2
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _connected
       (if _connected (write-byte -1 s) (write-byte 0 s))
     ;; int16 _temperature
       (write-word _temperature s)
     ;; int16 _voltage
       (write-word _voltage s)
     ;; int16 _current
       (write-word _current s)
     ;; int16 _remaining_capacity
       (write-word _remaining_capacity s)
     ;; int16 _cycles_count
       (write-word _cycles_count s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _connected
     (setq _connected (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; int16 _temperature
     (setq _temperature (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _voltage
     (setq _voltage (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _current
     (setq _current (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _remaining_capacity
     (setq _remaining_capacity (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; int16 _cycles_count
     (setq _cycles_count (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;;
   self)
  )

(setf (get gd_msgs::BatteryRegs :md5sum-) "823d1929afb53aaad11fd102584b3f5f")
(setf (get gd_msgs::BatteryRegs :datatype-) "gd_msgs/BatteryRegs")
(setf (get gd_msgs::BatteryRegs :definition-)
      "bool connected
int16 temperature
int16 voltage
int16 current
int16 remaining_capacity
int16 cycles_count


")



(provide :gd_msgs/BatteryRegs "823d1929afb53aaad11fd102584b3f5f")


