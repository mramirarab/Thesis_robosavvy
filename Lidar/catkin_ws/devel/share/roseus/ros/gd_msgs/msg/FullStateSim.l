;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::FullStateSim)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'FullStateSim (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::FULLSTATESIM")
  (make-package "GD_MSGS::FULLSTATESIM"))

(in-package "ROS")
;;//! \htmlinclude FullStateSim.msg.html


(defclass gd_msgs::FullStateSim
  :super ros::object
  :slots (_wip_state _desired_state _fdbk_state _u_eq_left _u_eq_right _phi_eq _u_left _u_right _w_left _w_right _roll _pitch _ramp_psi _ramp_delta ))

(defmethod gd_msgs::FullStateSim
  (:init
   (&key
    ((:wip_state __wip_state) (instance gd_msgs::State :init))
    ((:desired_state __desired_state) (instance gd_msgs::State :init))
    ((:fdbk_state __fdbk_state) (instance gd_msgs::State :init))
    ((:u_eq_left __u_eq_left) 0.0)
    ((:u_eq_right __u_eq_right) 0.0)
    ((:phi_eq __phi_eq) 0.0)
    ((:u_left __u_left) 0.0)
    ((:u_right __u_right) 0.0)
    ((:w_left __w_left) 0.0)
    ((:w_right __w_right) 0.0)
    ((:roll __roll) 0.0)
    ((:pitch __pitch) 0.0)
    ((:ramp_psi __ramp_psi) 0.0)
    ((:ramp_delta __ramp_delta) 0.0)
    )
   (send-super :init)
   (setq _wip_state __wip_state)
   (setq _desired_state __desired_state)
   (setq _fdbk_state __fdbk_state)
   (setq _u_eq_left (float __u_eq_left))
   (setq _u_eq_right (float __u_eq_right))
   (setq _phi_eq (float __phi_eq))
   (setq _u_left (float __u_left))
   (setq _u_right (float __u_right))
   (setq _w_left (float __w_left))
   (setq _w_right (float __w_right))
   (setq _roll (float __roll))
   (setq _pitch (float __pitch))
   (setq _ramp_psi (float __ramp_psi))
   (setq _ramp_delta (float __ramp_delta))
   self)
  (:wip_state
   (&rest __wip_state)
   (if (keywordp (car __wip_state))
       (send* _wip_state __wip_state)
     (progn
       (if __wip_state (setq _wip_state (car __wip_state)))
       _wip_state)))
  (:desired_state
   (&rest __desired_state)
   (if (keywordp (car __desired_state))
       (send* _desired_state __desired_state)
     (progn
       (if __desired_state (setq _desired_state (car __desired_state)))
       _desired_state)))
  (:fdbk_state
   (&rest __fdbk_state)
   (if (keywordp (car __fdbk_state))
       (send* _fdbk_state __fdbk_state)
     (progn
       (if __fdbk_state (setq _fdbk_state (car __fdbk_state)))
       _fdbk_state)))
  (:u_eq_left
   (&optional __u_eq_left)
   (if __u_eq_left (setq _u_eq_left __u_eq_left)) _u_eq_left)
  (:u_eq_right
   (&optional __u_eq_right)
   (if __u_eq_right (setq _u_eq_right __u_eq_right)) _u_eq_right)
  (:phi_eq
   (&optional __phi_eq)
   (if __phi_eq (setq _phi_eq __phi_eq)) _phi_eq)
  (:u_left
   (&optional __u_left)
   (if __u_left (setq _u_left __u_left)) _u_left)
  (:u_right
   (&optional __u_right)
   (if __u_right (setq _u_right __u_right)) _u_right)
  (:w_left
   (&optional __w_left)
   (if __w_left (setq _w_left __w_left)) _w_left)
  (:w_right
   (&optional __w_right)
   (if __w_right (setq _w_right __w_right)) _w_right)
  (:roll
   (&optional __roll)
   (if __roll (setq _roll __roll)) _roll)
  (:pitch
   (&optional __pitch)
   (if __pitch (setq _pitch __pitch)) _pitch)
  (:ramp_psi
   (&optional __ramp_psi)
   (if __ramp_psi (setq _ramp_psi __ramp_psi)) _ramp_psi)
  (:ramp_delta
   (&optional __ramp_delta)
   (if __ramp_delta (setq _ramp_delta __ramp_delta)) _ramp_delta)
  (:serialization-length
   ()
   (+
    ;; gd_msgs/State _wip_state
    (send _wip_state :serialization-length)
    ;; gd_msgs/State _desired_state
    (send _desired_state :serialization-length)
    ;; gd_msgs/State _fdbk_state
    (send _fdbk_state :serialization-length)
    ;; float32 _u_eq_left
    4
    ;; float32 _u_eq_right
    4
    ;; float32 _phi_eq
    4
    ;; float32 _u_left
    4
    ;; float32 _u_right
    4
    ;; float32 _w_left
    4
    ;; float32 _w_right
    4
    ;; float32 _roll
    4
    ;; float32 _pitch
    4
    ;; float32 _ramp_psi
    4
    ;; float32 _ramp_delta
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; gd_msgs/State _wip_state
       (send _wip_state :serialize s)
     ;; gd_msgs/State _desired_state
       (send _desired_state :serialize s)
     ;; gd_msgs/State _fdbk_state
       (send _fdbk_state :serialize s)
     ;; float32 _u_eq_left
       (sys::poke _u_eq_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _u_eq_right
       (sys::poke _u_eq_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _phi_eq
       (sys::poke _phi_eq (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _u_left
       (sys::poke _u_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _u_right
       (sys::poke _u_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_left
       (sys::poke _w_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_right
       (sys::poke _w_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _roll
       (sys::poke _roll (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _pitch
       (sys::poke _pitch (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ramp_psi
       (sys::poke _ramp_psi (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _ramp_delta
       (sys::poke _ramp_delta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; gd_msgs/State _wip_state
     (send _wip_state :deserialize buf ptr-) (incf ptr- (send _wip_state :serialization-length))
   ;; gd_msgs/State _desired_state
     (send _desired_state :deserialize buf ptr-) (incf ptr- (send _desired_state :serialization-length))
   ;; gd_msgs/State _fdbk_state
     (send _fdbk_state :deserialize buf ptr-) (incf ptr- (send _fdbk_state :serialization-length))
   ;; float32 _u_eq_left
     (setq _u_eq_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _u_eq_right
     (setq _u_eq_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _phi_eq
     (setq _phi_eq (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _u_left
     (setq _u_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _u_right
     (setq _u_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_left
     (setq _w_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_right
     (setq _w_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _roll
     (setq _roll (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _pitch
     (setq _pitch (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ramp_psi
     (setq _ramp_psi (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _ramp_delta
     (setq _ramp_delta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gd_msgs::FullStateSim :md5sum-) "8b31cdf4b9e337ab7b2e4b6fe00facc0")
(setf (get gd_msgs::FullStateSim :datatype-) "gd_msgs/FullStateSim")
(setf (get gd_msgs::FullStateSim :definition-)
      "# WIP State
State wip_state
State desired_state
State fdbk_state

# Equilibrium condition:
float32 u_eq_left
float32 u_eq_right
float32 phi_eq

# Current actuation:
float32 u_left
float32 u_right

# Current wheel velocity:
float32 w_left
float32 w_right

# IMU sensed:
float32 roll
float32 pitch

# Sensed floor:
float32 ramp_psi
float32 ramp_delta


================================================================================
MSG: gd_msgs/State
# State space vector for balance system
float32 phi
float32 dx
float32 dpsi
float32 dphi

")



(provide :gd_msgs/FullStateSim "8b31cdf4b9e337ab7b2e4b6fe00facc0")


