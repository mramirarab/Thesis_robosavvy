;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::FullStateVelT)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'FullStateVelT (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::FULLSTATEVELT")
  (make-package "GD_MSGS::FULLSTATEVELT"))

(in-package "ROS")
;;//! \htmlinclude FullStateVelT.msg.html


(defclass gd_msgs::FullStateVelT
  :super ros::object
  :slots (_t _wip_state _desired_state _fdbk_state _u_left _u_right _w_fbk_left _w_fbk_right _w_cmd_left _w_cmd_right ))

(defmethod gd_msgs::FullStateVelT
  (:init
   (&key
    ((:t __t) 0.0)
    ((:wip_state __wip_state) (instance gd_msgs::State :init))
    ((:desired_state __desired_state) (instance gd_msgs::State :init))
    ((:fdbk_state __fdbk_state) (instance gd_msgs::State :init))
    ((:u_left __u_left) 0.0)
    ((:u_right __u_right) 0.0)
    ((:w_fbk_left __w_fbk_left) 0.0)
    ((:w_fbk_right __w_fbk_right) 0.0)
    ((:w_cmd_left __w_cmd_left) 0.0)
    ((:w_cmd_right __w_cmd_right) 0.0)
    )
   (send-super :init)
   (setq _t (float __t))
   (setq _wip_state __wip_state)
   (setq _desired_state __desired_state)
   (setq _fdbk_state __fdbk_state)
   (setq _u_left (float __u_left))
   (setq _u_right (float __u_right))
   (setq _w_fbk_left (float __w_fbk_left))
   (setq _w_fbk_right (float __w_fbk_right))
   (setq _w_cmd_left (float __w_cmd_left))
   (setq _w_cmd_right (float __w_cmd_right))
   self)
  (:t
   (&optional __t)
   (if __t (setq _t __t)) _t)
  (:wip_state
   (&rest __wip_state)
   (if (keywordp (car __wip_state))
       (send* _wip_state __wip_state)
     (progn
       (if __wip_state (setq _wip_state (car __wip_state)))
       _wip_state)))
  (:desired_state
   (&rest __desired_state)
   (if (keywordp (car __desired_state))
       (send* _desired_state __desired_state)
     (progn
       (if __desired_state (setq _desired_state (car __desired_state)))
       _desired_state)))
  (:fdbk_state
   (&rest __fdbk_state)
   (if (keywordp (car __fdbk_state))
       (send* _fdbk_state __fdbk_state)
     (progn
       (if __fdbk_state (setq _fdbk_state (car __fdbk_state)))
       _fdbk_state)))
  (:u_left
   (&optional __u_left)
   (if __u_left (setq _u_left __u_left)) _u_left)
  (:u_right
   (&optional __u_right)
   (if __u_right (setq _u_right __u_right)) _u_right)
  (:w_fbk_left
   (&optional __w_fbk_left)
   (if __w_fbk_left (setq _w_fbk_left __w_fbk_left)) _w_fbk_left)
  (:w_fbk_right
   (&optional __w_fbk_right)
   (if __w_fbk_right (setq _w_fbk_right __w_fbk_right)) _w_fbk_right)
  (:w_cmd_left
   (&optional __w_cmd_left)
   (if __w_cmd_left (setq _w_cmd_left __w_cmd_left)) _w_cmd_left)
  (:w_cmd_right
   (&optional __w_cmd_right)
   (if __w_cmd_right (setq _w_cmd_right __w_cmd_right)) _w_cmd_right)
  (:serialization-length
   ()
   (+
    ;; float32 _t
    4
    ;; gd_msgs/State _wip_state
    (send _wip_state :serialization-length)
    ;; gd_msgs/State _desired_state
    (send _desired_state :serialization-length)
    ;; gd_msgs/State _fdbk_state
    (send _fdbk_state :serialization-length)
    ;; float32 _u_left
    4
    ;; float32 _u_right
    4
    ;; float32 _w_fbk_left
    4
    ;; float32 _w_fbk_right
    4
    ;; float32 _w_cmd_left
    4
    ;; float32 _w_cmd_right
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _t
       (sys::poke _t (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; gd_msgs/State _wip_state
       (send _wip_state :serialize s)
     ;; gd_msgs/State _desired_state
       (send _desired_state :serialize s)
     ;; gd_msgs/State _fdbk_state
       (send _fdbk_state :serialize s)
     ;; float32 _u_left
       (sys::poke _u_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _u_right
       (sys::poke _u_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_fbk_left
       (sys::poke _w_fbk_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_fbk_right
       (sys::poke _w_fbk_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_cmd_left
       (sys::poke _w_cmd_left (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _w_cmd_right
       (sys::poke _w_cmd_right (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _t
     (setq _t (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; gd_msgs/State _wip_state
     (send _wip_state :deserialize buf ptr-) (incf ptr- (send _wip_state :serialization-length))
   ;; gd_msgs/State _desired_state
     (send _desired_state :deserialize buf ptr-) (incf ptr- (send _desired_state :serialization-length))
   ;; gd_msgs/State _fdbk_state
     (send _fdbk_state :deserialize buf ptr-) (incf ptr- (send _fdbk_state :serialization-length))
   ;; float32 _u_left
     (setq _u_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _u_right
     (setq _u_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_fbk_left
     (setq _w_fbk_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_fbk_right
     (setq _w_fbk_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_cmd_left
     (setq _w_cmd_left (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _w_cmd_right
     (setq _w_cmd_right (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gd_msgs::FullStateVelT :md5sum-) "5e195cad1db7913ceef7f22bba0b753c")
(setf (get gd_msgs::FullStateVelT :datatype-) "gd_msgs/FullStateVelT")
(setf (get gd_msgs::FullStateVelT :definition-)
      "float32 t

# WIP State
State wip_state
State desired_state
State fdbk_state

# Current actuation:
float32 u_left
float32 u_right

# Current wheel velocity:
float32 w_fbk_left
float32 w_fbk_right

# Wheel command:
float32 w_cmd_left
float32 w_cmd_right

================================================================================
MSG: gd_msgs/State
# State space vector for balance system
float32 phi
float32 dx
float32 dpsi
float32 dphi

")



(provide :gd_msgs/FullStateVelT "5e195cad1db7913ceef7f22bba0b753c")


