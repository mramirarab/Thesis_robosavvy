;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::RCArray)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'RCArray (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::RCARRAY")
  (make-package "GD_MSGS::RCARRAY"))

(in-package "ROS")
;;//! \htmlinclude RCArray.msg.html


(defclass gd_msgs::RCArray
  :super ros::object
  :slots (_channel ))

(defmethod gd_msgs::RCArray
  (:init
   (&key
    ((:channel __channel) (let (r) (dotimes (i 14) (push (instance gd_msgs::RCValue :init) r)) r))
    )
   (send-super :init)
   (setq _channel __channel)
   self)
  (:channel
   (&rest __channel)
   (if (keywordp (car __channel))
       (send* _channel __channel)
     (progn
       (if __channel (setq _channel (car __channel)))
       _channel)))
  (:serialization-length
   ()
   (+
    ;; gd_msgs/RCValue[14] _channel
    (apply #'+ (send-all _channel :serialization-length))
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; gd_msgs/RCValue[14] _channel
     (dolist (elem _channel)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; gd_msgs/RCValue[14] _channel
   (dotimes (i 14)
     (send (elt _channel i) :deserialize buf ptr-) (incf ptr- (send (elt _channel i) :serialization-length))
     )
   ;;
   self)
  )

(setf (get gd_msgs::RCArray :md5sum-) "ed6b509efd13af7cb1c101e3d45fe032")
(setf (get gd_msgs::RCArray :datatype-) "gd_msgs/RCArray")
(setf (get gd_msgs::RCArray :definition-)
      "RCValue[14] channel

================================================================================
MSG: gd_msgs/RCValue
bool connected
int16 value

")



(provide :gd_msgs/RCArray "ed6b509efd13af7cb1c101e3d45fe032")


