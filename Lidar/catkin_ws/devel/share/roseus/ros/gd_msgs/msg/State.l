;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::State)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'State (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::STATE")
  (make-package "GD_MSGS::STATE"))

(in-package "ROS")
;;//! \htmlinclude State.msg.html


(defclass gd_msgs::State
  :super ros::object
  :slots (_phi _dx _dpsi _dphi ))

(defmethod gd_msgs::State
  (:init
   (&key
    ((:phi __phi) 0.0)
    ((:dx __dx) 0.0)
    ((:dpsi __dpsi) 0.0)
    ((:dphi __dphi) 0.0)
    )
   (send-super :init)
   (setq _phi (float __phi))
   (setq _dx (float __dx))
   (setq _dpsi (float __dpsi))
   (setq _dphi (float __dphi))
   self)
  (:phi
   (&optional __phi)
   (if __phi (setq _phi __phi)) _phi)
  (:dx
   (&optional __dx)
   (if __dx (setq _dx __dx)) _dx)
  (:dpsi
   (&optional __dpsi)
   (if __dpsi (setq _dpsi __dpsi)) _dpsi)
  (:dphi
   (&optional __dphi)
   (if __dphi (setq _dphi __dphi)) _dphi)
  (:serialization-length
   ()
   (+
    ;; float32 _phi
    4
    ;; float32 _dx
    4
    ;; float32 _dpsi
    4
    ;; float32 _dphi
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _phi
       (sys::poke _phi (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dx
       (sys::poke _dx (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dpsi
       (sys::poke _dpsi (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _dphi
       (sys::poke _dphi (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _phi
     (setq _phi (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dx
     (setq _dx (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dpsi
     (setq _dpsi (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _dphi
     (setq _dphi (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get gd_msgs::State :md5sum-) "925e410a821a0796fd3cb35b82889ab3")
(setf (get gd_msgs::State :datatype-) "gd_msgs/State")
(setf (get gd_msgs::State :definition-)
      "# State space vector for balance system
float32 phi
float32 dx
float32 dpsi
float32 dphi

")



(provide :gd_msgs/State "925e410a821a0796fd3cb35b82889ab3")


