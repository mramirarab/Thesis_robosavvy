;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::BatteryRegsArray)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'BatteryRegsArray (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::BATTERYREGSARRAY")
  (make-package "GD_MSGS::BATTERYREGSARRAY"))

(in-package "ROS")
;;//! \htmlinclude BatteryRegsArray.msg.html


(defclass gd_msgs::BatteryRegsArray
  :super ros::object
  :slots (_battery ))

(defmethod gd_msgs::BatteryRegsArray
  (:init
   (&key
    ((:battery __battery) (let (r) (dotimes (i 2) (push (instance gd_msgs::BatteryRegs :init) r)) r))
    )
   (send-super :init)
   (setq _battery __battery)
   self)
  (:battery
   (&rest __battery)
   (if (keywordp (car __battery))
       (send* _battery __battery)
     (progn
       (if __battery (setq _battery (car __battery)))
       _battery)))
  (:serialization-length
   ()
   (+
    ;; gd_msgs/BatteryRegs[2] _battery
    (apply #'+ (send-all _battery :serialization-length))
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; gd_msgs/BatteryRegs[2] _battery
     (dolist (elem _battery)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; gd_msgs/BatteryRegs[2] _battery
   (dotimes (i 2)
     (send (elt _battery i) :deserialize buf ptr-) (incf ptr- (send (elt _battery i) :serialization-length))
     )
   ;;
   self)
  )

(setf (get gd_msgs::BatteryRegsArray :md5sum-) "088a6a0e1bf795cb18e5c5f436e1d837")
(setf (get gd_msgs::BatteryRegsArray :datatype-) "gd_msgs/BatteryRegsArray")
(setf (get gd_msgs::BatteryRegsArray :definition-)
      "BatteryRegs[2] battery

================================================================================
MSG: gd_msgs/BatteryRegs
bool connected
int16 temperature
int16 voltage
int16 current
int16 remaining_capacity
int16 cycles_count


")



(provide :gd_msgs/BatteryRegsArray "088a6a0e1bf795cb18e5c5f436e1d837")


