;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::SetInput)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'SetInput (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::SETINPUT")
  (make-package "GD_MSGS::SETINPUT"))
(unless (find-package "GD_MSGS::SETINPUTREQUEST")
  (make-package "GD_MSGS::SETINPUTREQUEST"))
(unless (find-package "GD_MSGS::SETINPUTRESPONSE")
  (make-package "GD_MSGS::SETINPUTRESPONSE"))

(in-package "ROS")





(intern "*PC*" (find-package "GD_MSGS::SETINPUTREQUEST"))
(shadow '*PC* (find-package "GD_MSGS::SETINPUTREQUEST"))
(defconstant gd_msgs::SetInputRequest::*PC* 0)
(intern "*RC*" (find-package "GD_MSGS::SETINPUTREQUEST"))
(shadow '*RC* (find-package "GD_MSGS::SETINPUTREQUEST"))
(defconstant gd_msgs::SetInputRequest::*RC* 1)
(intern "*MIX*" (find-package "GD_MSGS::SETINPUTREQUEST"))
(shadow '*MIX* (find-package "GD_MSGS::SETINPUTREQUEST"))
(defconstant gd_msgs::SetInputRequest::*MIX* 2)
(defclass gd_msgs::SetInputRequest
  :super ros::object
  :slots (_input ))

(defmethod gd_msgs::SetInputRequest
  (:init
   (&key
    ((:input __input) 0)
    )
   (send-super :init)
   (setq _input (round __input))
   self)
  (:input
   (&optional __input)
   (if __input (setq _input __input)) _input)
  (:serialization-length
   ()
   (+
    ;; int32 _input
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _input
       (write-long _input s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _input
     (setq _input (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass gd_msgs::SetInputResponse
  :super ros::object
  :slots ())

(defmethod gd_msgs::SetInputResponse
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass gd_msgs::SetInput
  :super ros::object
  :slots ())

(setf (get gd_msgs::SetInput :md5sum-) "d624d0a06e7d9cd337fb3219e1a659d5")
(setf (get gd_msgs::SetInput :datatype-) "gd_msgs/SetInput")
(setf (get gd_msgs::SetInput :request) gd_msgs::SetInputRequest)
(setf (get gd_msgs::SetInput :response) gd_msgs::SetInputResponse)

(defmethod gd_msgs::SetInputRequest
  (:response () (instance gd_msgs::SetInputResponse :init)))

(setf (get gd_msgs::SetInputRequest :md5sum-) "d624d0a06e7d9cd337fb3219e1a659d5")
(setf (get gd_msgs::SetInputRequest :datatype-) "gd_msgs/SetInputRequest")
(setf (get gd_msgs::SetInputRequest :definition-)
      "






int32 PC = 0
int32 RC = 1
int32 MIX = 2


int32 input
---

")

(setf (get gd_msgs::SetInputResponse :md5sum-) "d624d0a06e7d9cd337fb3219e1a659d5")
(setf (get gd_msgs::SetInputResponse :datatype-) "gd_msgs/SetInputResponse")
(setf (get gd_msgs::SetInputResponse :definition-)
      "






int32 PC = 0
int32 RC = 1
int32 MIX = 2


int32 input
---

")



(provide :gd_msgs/SetInput "d624d0a06e7d9cd337fb3219e1a659d5")


