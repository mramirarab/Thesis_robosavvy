;; Auto-generated. Do not edit!


(when (boundp 'gd_msgs::SetMode)
  (if (not (find-package "GD_MSGS"))
    (make-package "GD_MSGS"))
  (shadow 'SetMode (find-package "GD_MSGS")))
(unless (find-package "GD_MSGS::SETMODE")
  (make-package "GD_MSGS::SETMODE"))
(unless (find-package "GD_MSGS::SETMODEREQUEST")
  (make-package "GD_MSGS::SETMODEREQUEST"))
(unless (find-package "GD_MSGS::SETMODERESPONSE")
  (make-package "GD_MSGS::SETMODERESPONSE"))

(in-package "ROS")





(intern "*PARK*" (find-package "GD_MSGS::SETMODEREQUEST"))
(shadow '*PARK* (find-package "GD_MSGS::SETMODEREQUEST"))
(defconstant gd_msgs::SetModeRequest::*PARK* 1)
(intern "*TRACTOR*" (find-package "GD_MSGS::SETMODEREQUEST"))
(shadow '*TRACTOR* (find-package "GD_MSGS::SETMODEREQUEST"))
(defconstant gd_msgs::SetModeRequest::*TRACTOR* 2)
(intern "*BALANCE*" (find-package "GD_MSGS::SETMODEREQUEST"))
(shadow '*BALANCE* (find-package "GD_MSGS::SETMODEREQUEST"))
(defconstant gd_msgs::SetModeRequest::*BALANCE* 3)
(intern "*DTZ*" (find-package "GD_MSGS::SETMODEREQUEST"))
(shadow '*DTZ* (find-package "GD_MSGS::SETMODEREQUEST"))
(defconstant gd_msgs::SetModeRequest::*DTZ* 4)
(defclass gd_msgs::SetModeRequest
  :super ros::object
  :slots (_mode ))

(defmethod gd_msgs::SetModeRequest
  (:init
   (&key
    ((:mode __mode) 0)
    )
   (send-super :init)
   (setq _mode (round __mode))
   self)
  (:mode
   (&optional __mode)
   (if __mode (setq _mode __mode)) _mode)
  (:serialization-length
   ()
   (+
    ;; int32 _mode
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _mode
       (write-long _mode s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _mode
     (setq _mode (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(defclass gd_msgs::SetModeResponse
  :super ros::object
  :slots (_success ))

(defmethod gd_msgs::SetModeResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional __success)
   (if __success (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass gd_msgs::SetMode
  :super ros::object
  :slots ())

(setf (get gd_msgs::SetMode :md5sum-) "478a3b435e96c6326a1a2d899849552d")
(setf (get gd_msgs::SetMode :datatype-) "gd_msgs/SetMode")
(setf (get gd_msgs::SetMode :request) gd_msgs::SetModeRequest)
(setf (get gd_msgs::SetMode :response) gd_msgs::SetModeResponse)

(defmethod gd_msgs::SetModeRequest
  (:response () (instance gd_msgs::SetModeResponse :init)))

(setf (get gd_msgs::SetModeRequest :md5sum-) "478a3b435e96c6326a1a2d899849552d")
(setf (get gd_msgs::SetModeRequest :datatype-) "gd_msgs/SetModeRequest")
(setf (get gd_msgs::SetModeRequest :definition-)
      "









int32 PARK = 1
int32 TRACTOR = 2
int32 BALANCE = 3
int32 DTZ = 4


int32 mode
---
bool success

")

(setf (get gd_msgs::SetModeResponse :md5sum-) "478a3b435e96c6326a1a2d899849552d")
(setf (get gd_msgs::SetModeResponse :datatype-) "gd_msgs/SetModeResponse")
(setf (get gd_msgs::SetModeResponse :definition-)
      "









int32 PARK = 1
int32 TRACTOR = 2
int32 BALANCE = 3
int32 DTZ = 4


int32 mode
---
bool success

")



(provide :gd_msgs/SetMode "478a3b435e96c6326a1a2d899849552d")


