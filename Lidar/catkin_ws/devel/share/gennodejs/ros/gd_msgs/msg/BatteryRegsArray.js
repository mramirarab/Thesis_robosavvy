// Auto-generated. Do not edit!

// (in-package gd_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let BatteryRegs = require('./BatteryRegs.js');

//-----------------------------------------------------------

class BatteryRegsArray {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.battery = null;
    }
    else {
      if (initObj.hasOwnProperty('battery')) {
        this.battery = initObj.battery
      }
      else {
        this.battery = new Array(2).fill(new BatteryRegs());
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type BatteryRegsArray
    // Check that the constant length array field [battery] has the right length
    if (obj.battery.length !== 2) {
      throw new Error('Unable to serialize array field battery - length must be 2')
    }
    // Serialize message field [battery]
    obj.battery.forEach((val) => {
      bufferOffset = BatteryRegs.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type BatteryRegsArray
    let len;
    let data = new BatteryRegsArray(null);
    // Deserialize message field [battery]
    len = 2;
    data.battery = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.battery[i] = BatteryRegs.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    return 11;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gd_msgs/BatteryRegsArray';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '088a6a0e1bf795cb18e5c5f436e1d837';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    BatteryRegs[2] battery
    
    ================================================================================
    MSG: gd_msgs/BatteryRegs
    bool connected
    int16 temperature
    int16 voltage
    int16 current
    int16 remaining_capacity
    int16 cycles_count
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new BatteryRegsArray(null);
    if (msg.battery !== undefined) {
      resolved.battery = new Array(2)
      for (let i = 0; i < resolved.battery.length; ++i) {
        if (msg.battery.length > i) {
          resolved.battery[i] = BatteryRegs.Resolve(msg.battery[i]);
        }
        else {
          resolved.battery[i] = new BatteryRegs();
        }
      }
    }
    else {
      resolved.battery = new Array(2).fill(new BatteryRegs())
    }

    return resolved;
    }
};

module.exports = BatteryRegsArray;
