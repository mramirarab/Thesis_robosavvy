// Auto-generated. Do not edit!

// (in-package gd_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let State = require('./State.js');

//-----------------------------------------------------------

class FullStateSim {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.wip_state = null;
      this.desired_state = null;
      this.fdbk_state = null;
      this.u_eq_left = null;
      this.u_eq_right = null;
      this.phi_eq = null;
      this.u_left = null;
      this.u_right = null;
      this.w_left = null;
      this.w_right = null;
      this.roll = null;
      this.pitch = null;
      this.ramp_psi = null;
      this.ramp_delta = null;
    }
    else {
      if (initObj.hasOwnProperty('wip_state')) {
        this.wip_state = initObj.wip_state
      }
      else {
        this.wip_state = new State();
      }
      if (initObj.hasOwnProperty('desired_state')) {
        this.desired_state = initObj.desired_state
      }
      else {
        this.desired_state = new State();
      }
      if (initObj.hasOwnProperty('fdbk_state')) {
        this.fdbk_state = initObj.fdbk_state
      }
      else {
        this.fdbk_state = new State();
      }
      if (initObj.hasOwnProperty('u_eq_left')) {
        this.u_eq_left = initObj.u_eq_left
      }
      else {
        this.u_eq_left = 0.0;
      }
      if (initObj.hasOwnProperty('u_eq_right')) {
        this.u_eq_right = initObj.u_eq_right
      }
      else {
        this.u_eq_right = 0.0;
      }
      if (initObj.hasOwnProperty('phi_eq')) {
        this.phi_eq = initObj.phi_eq
      }
      else {
        this.phi_eq = 0.0;
      }
      if (initObj.hasOwnProperty('u_left')) {
        this.u_left = initObj.u_left
      }
      else {
        this.u_left = 0.0;
      }
      if (initObj.hasOwnProperty('u_right')) {
        this.u_right = initObj.u_right
      }
      else {
        this.u_right = 0.0;
      }
      if (initObj.hasOwnProperty('w_left')) {
        this.w_left = initObj.w_left
      }
      else {
        this.w_left = 0.0;
      }
      if (initObj.hasOwnProperty('w_right')) {
        this.w_right = initObj.w_right
      }
      else {
        this.w_right = 0.0;
      }
      if (initObj.hasOwnProperty('roll')) {
        this.roll = initObj.roll
      }
      else {
        this.roll = 0.0;
      }
      if (initObj.hasOwnProperty('pitch')) {
        this.pitch = initObj.pitch
      }
      else {
        this.pitch = 0.0;
      }
      if (initObj.hasOwnProperty('ramp_psi')) {
        this.ramp_psi = initObj.ramp_psi
      }
      else {
        this.ramp_psi = 0.0;
      }
      if (initObj.hasOwnProperty('ramp_delta')) {
        this.ramp_delta = initObj.ramp_delta
      }
      else {
        this.ramp_delta = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type FullStateSim
    // Serialize message field [wip_state]
    bufferOffset = State.serialize(obj.wip_state, buffer, bufferOffset);
    // Serialize message field [desired_state]
    bufferOffset = State.serialize(obj.desired_state, buffer, bufferOffset);
    // Serialize message field [fdbk_state]
    bufferOffset = State.serialize(obj.fdbk_state, buffer, bufferOffset);
    // Serialize message field [u_eq_left]
    bufferOffset = _serializer.float32(obj.u_eq_left, buffer, bufferOffset);
    // Serialize message field [u_eq_right]
    bufferOffset = _serializer.float32(obj.u_eq_right, buffer, bufferOffset);
    // Serialize message field [phi_eq]
    bufferOffset = _serializer.float32(obj.phi_eq, buffer, bufferOffset);
    // Serialize message field [u_left]
    bufferOffset = _serializer.float32(obj.u_left, buffer, bufferOffset);
    // Serialize message field [u_right]
    bufferOffset = _serializer.float32(obj.u_right, buffer, bufferOffset);
    // Serialize message field [w_left]
    bufferOffset = _serializer.float32(obj.w_left, buffer, bufferOffset);
    // Serialize message field [w_right]
    bufferOffset = _serializer.float32(obj.w_right, buffer, bufferOffset);
    // Serialize message field [roll]
    bufferOffset = _serializer.float32(obj.roll, buffer, bufferOffset);
    // Serialize message field [pitch]
    bufferOffset = _serializer.float32(obj.pitch, buffer, bufferOffset);
    // Serialize message field [ramp_psi]
    bufferOffset = _serializer.float32(obj.ramp_psi, buffer, bufferOffset);
    // Serialize message field [ramp_delta]
    bufferOffset = _serializer.float32(obj.ramp_delta, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type FullStateSim
    let len;
    let data = new FullStateSim(null);
    // Deserialize message field [wip_state]
    data.wip_state = State.deserialize(buffer, bufferOffset);
    // Deserialize message field [desired_state]
    data.desired_state = State.deserialize(buffer, bufferOffset);
    // Deserialize message field [fdbk_state]
    data.fdbk_state = State.deserialize(buffer, bufferOffset);
    // Deserialize message field [u_eq_left]
    data.u_eq_left = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [u_eq_right]
    data.u_eq_right = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [phi_eq]
    data.phi_eq = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [u_left]
    data.u_left = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [u_right]
    data.u_right = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_left]
    data.w_left = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_right]
    data.w_right = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [roll]
    data.roll = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [pitch]
    data.pitch = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [ramp_psi]
    data.ramp_psi = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [ramp_delta]
    data.ramp_delta = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 92;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gd_msgs/FullStateSim';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '8b31cdf4b9e337ab7b2e4b6fe00facc0';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # WIP State
    State wip_state
    State desired_state
    State fdbk_state
    
    # Equilibrium condition:
    float32 u_eq_left
    float32 u_eq_right
    float32 phi_eq
    
    # Current actuation:
    float32 u_left
    float32 u_right
    
    # Current wheel velocity:
    float32 w_left
    float32 w_right
    
    # IMU sensed:
    float32 roll
    float32 pitch
    
    # Sensed floor:
    float32 ramp_psi
    float32 ramp_delta
    
    
    ================================================================================
    MSG: gd_msgs/State
    # State space vector for balance system
    float32 phi
    float32 dx
    float32 dpsi
    float32 dphi
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new FullStateSim(null);
    if (msg.wip_state !== undefined) {
      resolved.wip_state = State.Resolve(msg.wip_state)
    }
    else {
      resolved.wip_state = new State()
    }

    if (msg.desired_state !== undefined) {
      resolved.desired_state = State.Resolve(msg.desired_state)
    }
    else {
      resolved.desired_state = new State()
    }

    if (msg.fdbk_state !== undefined) {
      resolved.fdbk_state = State.Resolve(msg.fdbk_state)
    }
    else {
      resolved.fdbk_state = new State()
    }

    if (msg.u_eq_left !== undefined) {
      resolved.u_eq_left = msg.u_eq_left;
    }
    else {
      resolved.u_eq_left = 0.0
    }

    if (msg.u_eq_right !== undefined) {
      resolved.u_eq_right = msg.u_eq_right;
    }
    else {
      resolved.u_eq_right = 0.0
    }

    if (msg.phi_eq !== undefined) {
      resolved.phi_eq = msg.phi_eq;
    }
    else {
      resolved.phi_eq = 0.0
    }

    if (msg.u_left !== undefined) {
      resolved.u_left = msg.u_left;
    }
    else {
      resolved.u_left = 0.0
    }

    if (msg.u_right !== undefined) {
      resolved.u_right = msg.u_right;
    }
    else {
      resolved.u_right = 0.0
    }

    if (msg.w_left !== undefined) {
      resolved.w_left = msg.w_left;
    }
    else {
      resolved.w_left = 0.0
    }

    if (msg.w_right !== undefined) {
      resolved.w_right = msg.w_right;
    }
    else {
      resolved.w_right = 0.0
    }

    if (msg.roll !== undefined) {
      resolved.roll = msg.roll;
    }
    else {
      resolved.roll = 0.0
    }

    if (msg.pitch !== undefined) {
      resolved.pitch = msg.pitch;
    }
    else {
      resolved.pitch = 0.0
    }

    if (msg.ramp_psi !== undefined) {
      resolved.ramp_psi = msg.ramp_psi;
    }
    else {
      resolved.ramp_psi = 0.0
    }

    if (msg.ramp_delta !== undefined) {
      resolved.ramp_delta = msg.ramp_delta;
    }
    else {
      resolved.ramp_delta = 0.0
    }

    return resolved;
    }
};

module.exports = FullStateSim;
