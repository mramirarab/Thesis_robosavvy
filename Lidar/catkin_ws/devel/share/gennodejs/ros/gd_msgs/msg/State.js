// Auto-generated. Do not edit!

// (in-package gd_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class State {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.phi = null;
      this.dx = null;
      this.dpsi = null;
      this.dphi = null;
    }
    else {
      if (initObj.hasOwnProperty('phi')) {
        this.phi = initObj.phi
      }
      else {
        this.phi = 0.0;
      }
      if (initObj.hasOwnProperty('dx')) {
        this.dx = initObj.dx
      }
      else {
        this.dx = 0.0;
      }
      if (initObj.hasOwnProperty('dpsi')) {
        this.dpsi = initObj.dpsi
      }
      else {
        this.dpsi = 0.0;
      }
      if (initObj.hasOwnProperty('dphi')) {
        this.dphi = initObj.dphi
      }
      else {
        this.dphi = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type State
    // Serialize message field [phi]
    bufferOffset = _serializer.float32(obj.phi, buffer, bufferOffset);
    // Serialize message field [dx]
    bufferOffset = _serializer.float32(obj.dx, buffer, bufferOffset);
    // Serialize message field [dpsi]
    bufferOffset = _serializer.float32(obj.dpsi, buffer, bufferOffset);
    // Serialize message field [dphi]
    bufferOffset = _serializer.float32(obj.dphi, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type State
    let len;
    let data = new State(null);
    // Deserialize message field [phi]
    data.phi = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dx]
    data.dx = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dpsi]
    data.dpsi = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [dphi]
    data.dphi = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 16;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gd_msgs/State';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '925e410a821a0796fd3cb35b82889ab3';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    # State space vector for balance system
    float32 phi
    float32 dx
    float32 dpsi
    float32 dphi
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new State(null);
    if (msg.phi !== undefined) {
      resolved.phi = msg.phi;
    }
    else {
      resolved.phi = 0.0
    }

    if (msg.dx !== undefined) {
      resolved.dx = msg.dx;
    }
    else {
      resolved.dx = 0.0
    }

    if (msg.dpsi !== undefined) {
      resolved.dpsi = msg.dpsi;
    }
    else {
      resolved.dpsi = 0.0
    }

    if (msg.dphi !== undefined) {
      resolved.dphi = msg.dphi;
    }
    else {
      resolved.dphi = 0.0
    }

    return resolved;
    }
};

module.exports = State;
