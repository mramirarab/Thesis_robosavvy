// Auto-generated. Do not edit!

// (in-package gd_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let RCValue = require('./RCValue.js');

//-----------------------------------------------------------

class RCArray {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.channel = null;
    }
    else {
      if (initObj.hasOwnProperty('channel')) {
        this.channel = initObj.channel
      }
      else {
        this.channel = new Array(14).fill(new RCValue());
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RCArray
    // Check that the constant length array field [channel] has the right length
    if (obj.channel.length !== 14) {
      throw new Error('Unable to serialize array field channel - length must be 14')
    }
    // Serialize message field [channel]
    obj.channel.forEach((val) => {
      bufferOffset = RCValue.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RCArray
    let len;
    let data = new RCArray(null);
    // Deserialize message field [channel]
    len = 14;
    data.channel = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.channel[i] = RCValue.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    return 3;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gd_msgs/RCArray';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'ed6b509efd13af7cb1c101e3d45fe032';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    RCValue[14] channel
    
    ================================================================================
    MSG: gd_msgs/RCValue
    bool connected
    int16 value
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RCArray(null);
    if (msg.channel !== undefined) {
      resolved.channel = new Array(14)
      for (let i = 0; i < resolved.channel.length; ++i) {
        if (msg.channel.length > i) {
          resolved.channel[i] = RCValue.Resolve(msg.channel[i]);
        }
        else {
          resolved.channel[i] = new RCValue();
        }
      }
    }
    else {
      resolved.channel = new Array(14).fill(new RCValue())
    }

    return resolved;
    }
};

module.exports = RCArray;
