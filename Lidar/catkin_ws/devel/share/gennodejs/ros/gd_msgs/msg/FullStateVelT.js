// Auto-generated. Do not edit!

// (in-package gd_msgs.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let State = require('./State.js');

//-----------------------------------------------------------

class FullStateVelT {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.t = null;
      this.wip_state = null;
      this.desired_state = null;
      this.fdbk_state = null;
      this.u_left = null;
      this.u_right = null;
      this.w_fbk_left = null;
      this.w_fbk_right = null;
      this.w_cmd_left = null;
      this.w_cmd_right = null;
    }
    else {
      if (initObj.hasOwnProperty('t')) {
        this.t = initObj.t
      }
      else {
        this.t = 0.0;
      }
      if (initObj.hasOwnProperty('wip_state')) {
        this.wip_state = initObj.wip_state
      }
      else {
        this.wip_state = new State();
      }
      if (initObj.hasOwnProperty('desired_state')) {
        this.desired_state = initObj.desired_state
      }
      else {
        this.desired_state = new State();
      }
      if (initObj.hasOwnProperty('fdbk_state')) {
        this.fdbk_state = initObj.fdbk_state
      }
      else {
        this.fdbk_state = new State();
      }
      if (initObj.hasOwnProperty('u_left')) {
        this.u_left = initObj.u_left
      }
      else {
        this.u_left = 0.0;
      }
      if (initObj.hasOwnProperty('u_right')) {
        this.u_right = initObj.u_right
      }
      else {
        this.u_right = 0.0;
      }
      if (initObj.hasOwnProperty('w_fbk_left')) {
        this.w_fbk_left = initObj.w_fbk_left
      }
      else {
        this.w_fbk_left = 0.0;
      }
      if (initObj.hasOwnProperty('w_fbk_right')) {
        this.w_fbk_right = initObj.w_fbk_right
      }
      else {
        this.w_fbk_right = 0.0;
      }
      if (initObj.hasOwnProperty('w_cmd_left')) {
        this.w_cmd_left = initObj.w_cmd_left
      }
      else {
        this.w_cmd_left = 0.0;
      }
      if (initObj.hasOwnProperty('w_cmd_right')) {
        this.w_cmd_right = initObj.w_cmd_right
      }
      else {
        this.w_cmd_right = 0.0;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type FullStateVelT
    // Serialize message field [t]
    bufferOffset = _serializer.float32(obj.t, buffer, bufferOffset);
    // Serialize message field [wip_state]
    bufferOffset = State.serialize(obj.wip_state, buffer, bufferOffset);
    // Serialize message field [desired_state]
    bufferOffset = State.serialize(obj.desired_state, buffer, bufferOffset);
    // Serialize message field [fdbk_state]
    bufferOffset = State.serialize(obj.fdbk_state, buffer, bufferOffset);
    // Serialize message field [u_left]
    bufferOffset = _serializer.float32(obj.u_left, buffer, bufferOffset);
    // Serialize message field [u_right]
    bufferOffset = _serializer.float32(obj.u_right, buffer, bufferOffset);
    // Serialize message field [w_fbk_left]
    bufferOffset = _serializer.float32(obj.w_fbk_left, buffer, bufferOffset);
    // Serialize message field [w_fbk_right]
    bufferOffset = _serializer.float32(obj.w_fbk_right, buffer, bufferOffset);
    // Serialize message field [w_cmd_left]
    bufferOffset = _serializer.float32(obj.w_cmd_left, buffer, bufferOffset);
    // Serialize message field [w_cmd_right]
    bufferOffset = _serializer.float32(obj.w_cmd_right, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type FullStateVelT
    let len;
    let data = new FullStateVelT(null);
    // Deserialize message field [t]
    data.t = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [wip_state]
    data.wip_state = State.deserialize(buffer, bufferOffset);
    // Deserialize message field [desired_state]
    data.desired_state = State.deserialize(buffer, bufferOffset);
    // Deserialize message field [fdbk_state]
    data.fdbk_state = State.deserialize(buffer, bufferOffset);
    // Deserialize message field [u_left]
    data.u_left = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [u_right]
    data.u_right = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_fbk_left]
    data.w_fbk_left = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_fbk_right]
    data.w_fbk_right = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_cmd_left]
    data.w_cmd_left = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [w_cmd_right]
    data.w_cmd_right = _deserializer.float32(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 76;
  }

  static datatype() {
    // Returns string type for a message object
    return 'gd_msgs/FullStateVelT';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '5e195cad1db7913ceef7f22bba0b753c';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 t
    
    # WIP State
    State wip_state
    State desired_state
    State fdbk_state
    
    # Current actuation:
    float32 u_left
    float32 u_right
    
    # Current wheel velocity:
    float32 w_fbk_left
    float32 w_fbk_right
    
    # Wheel command:
    float32 w_cmd_left
    float32 w_cmd_right
    
    ================================================================================
    MSG: gd_msgs/State
    # State space vector for balance system
    float32 phi
    float32 dx
    float32 dpsi
    float32 dphi
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new FullStateVelT(null);
    if (msg.t !== undefined) {
      resolved.t = msg.t;
    }
    else {
      resolved.t = 0.0
    }

    if (msg.wip_state !== undefined) {
      resolved.wip_state = State.Resolve(msg.wip_state)
    }
    else {
      resolved.wip_state = new State()
    }

    if (msg.desired_state !== undefined) {
      resolved.desired_state = State.Resolve(msg.desired_state)
    }
    else {
      resolved.desired_state = new State()
    }

    if (msg.fdbk_state !== undefined) {
      resolved.fdbk_state = State.Resolve(msg.fdbk_state)
    }
    else {
      resolved.fdbk_state = new State()
    }

    if (msg.u_left !== undefined) {
      resolved.u_left = msg.u_left;
    }
    else {
      resolved.u_left = 0.0
    }

    if (msg.u_right !== undefined) {
      resolved.u_right = msg.u_right;
    }
    else {
      resolved.u_right = 0.0
    }

    if (msg.w_fbk_left !== undefined) {
      resolved.w_fbk_left = msg.w_fbk_left;
    }
    else {
      resolved.w_fbk_left = 0.0
    }

    if (msg.w_fbk_right !== undefined) {
      resolved.w_fbk_right = msg.w_fbk_right;
    }
    else {
      resolved.w_fbk_right = 0.0
    }

    if (msg.w_cmd_left !== undefined) {
      resolved.w_cmd_left = msg.w_cmd_left;
    }
    else {
      resolved.w_cmd_left = 0.0
    }

    if (msg.w_cmd_right !== undefined) {
      resolved.w_cmd_right = msg.w_cmd_right;
    }
    else {
      resolved.w_cmd_right = 0.0
    }

    return resolved;
    }
};

module.exports = FullStateVelT;
