
"use strict";

let SetInput = require('./SetInput.js')
let SetMode = require('./SetMode.js')

module.exports = {
  SetInput: SetInput,
  SetMode: SetMode,
};
