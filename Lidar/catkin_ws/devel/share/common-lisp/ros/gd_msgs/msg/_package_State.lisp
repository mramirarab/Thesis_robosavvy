(cl:in-package gd_msgs-msg)
(cl:export '(PHI-VAL
          PHI
          DX-VAL
          DX
          DPSI-VAL
          DPSI
          DPHI-VAL
          DPHI
))