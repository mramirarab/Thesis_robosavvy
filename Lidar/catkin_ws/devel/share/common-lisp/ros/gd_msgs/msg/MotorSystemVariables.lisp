; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude MotorSystemVariables.msg.html

(cl:defclass <MotorSystemVariables> (roslisp-msg-protocol:ros-message)
  ((position
    :reader position
    :initarg :position
    :type cl:float
    :initform 0.0)
   (velocity
    :reader velocity
    :initarg :velocity
    :type cl:float
    :initform 0.0)
   (k
    :reader k
    :initarg :k
    :type cl:float
    :initform 0.0)
   (ki
    :reader ki
    :initarg :ki
    :type cl:float
    :initform 0.0)
   (kd
    :reader kd
    :initarg :kd
    :type cl:float
    :initform 0.0))
)

(cl:defclass MotorSystemVariables (<MotorSystemVariables>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <MotorSystemVariables>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'MotorSystemVariables)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<MotorSystemVariables> is deprecated: use gd_msgs-msg:MotorSystemVariables instead.")))

(cl:ensure-generic-function 'position-val :lambda-list '(m))
(cl:defmethod position-val ((m <MotorSystemVariables>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:position-val is deprecated.  Use gd_msgs-msg:position instead.")
  (position m))

(cl:ensure-generic-function 'velocity-val :lambda-list '(m))
(cl:defmethod velocity-val ((m <MotorSystemVariables>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:velocity-val is deprecated.  Use gd_msgs-msg:velocity instead.")
  (velocity m))

(cl:ensure-generic-function 'k-val :lambda-list '(m))
(cl:defmethod k-val ((m <MotorSystemVariables>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:k-val is deprecated.  Use gd_msgs-msg:k instead.")
  (k m))

(cl:ensure-generic-function 'ki-val :lambda-list '(m))
(cl:defmethod ki-val ((m <MotorSystemVariables>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:ki-val is deprecated.  Use gd_msgs-msg:ki instead.")
  (ki m))

(cl:ensure-generic-function 'kd-val :lambda-list '(m))
(cl:defmethod kd-val ((m <MotorSystemVariables>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:kd-val is deprecated.  Use gd_msgs-msg:kd instead.")
  (kd m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <MotorSystemVariables>) ostream)
  "Serializes a message object of type '<MotorSystemVariables>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'position))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'velocity))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'k))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ki))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'kd))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <MotorSystemVariables>) istream)
  "Deserializes a message object of type '<MotorSystemVariables>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'position) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'velocity) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'k) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ki) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'kd) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<MotorSystemVariables>)))
  "Returns string type for a message object of type '<MotorSystemVariables>"
  "gd_msgs/MotorSystemVariables")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'MotorSystemVariables)))
  "Returns string type for a message object of type 'MotorSystemVariables"
  "gd_msgs/MotorSystemVariables")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<MotorSystemVariables>)))
  "Returns md5sum for a message object of type '<MotorSystemVariables>"
  "ad727b23c5a39de4b0eda6c33227bf59")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'MotorSystemVariables)))
  "Returns md5sum for a message object of type 'MotorSystemVariables"
  "ad727b23c5a39de4b0eda6c33227bf59")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<MotorSystemVariables>)))
  "Returns full string definition for message of type '<MotorSystemVariables>"
  (cl:format cl:nil "float32 position~%float32 velocity~%float32 k~%float32 ki~%float32 kd~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'MotorSystemVariables)))
  "Returns full string definition for message of type 'MotorSystemVariables"
  (cl:format cl:nil "float32 position~%float32 velocity~%float32 k~%float32 ki~%float32 kd~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <MotorSystemVariables>))
  (cl:+ 0
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <MotorSystemVariables>))
  "Converts a ROS message object to a list"
  (cl:list 'MotorSystemVariables
    (cl:cons ':position (position msg))
    (cl:cons ':velocity (velocity msg))
    (cl:cons ':k (k msg))
    (cl:cons ':ki (ki msg))
    (cl:cons ':kd (kd msg))
))
