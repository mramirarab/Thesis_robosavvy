; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude FullStateSim.msg.html

(cl:defclass <FullStateSim> (roslisp-msg-protocol:ros-message)
  ((wip_state
    :reader wip_state
    :initarg :wip_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (desired_state
    :reader desired_state
    :initarg :desired_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (fdbk_state
    :reader fdbk_state
    :initarg :fdbk_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (u_eq_left
    :reader u_eq_left
    :initarg :u_eq_left
    :type cl:float
    :initform 0.0)
   (u_eq_right
    :reader u_eq_right
    :initarg :u_eq_right
    :type cl:float
    :initform 0.0)
   (phi_eq
    :reader phi_eq
    :initarg :phi_eq
    :type cl:float
    :initform 0.0)
   (u_left
    :reader u_left
    :initarg :u_left
    :type cl:float
    :initform 0.0)
   (u_right
    :reader u_right
    :initarg :u_right
    :type cl:float
    :initform 0.0)
   (w_left
    :reader w_left
    :initarg :w_left
    :type cl:float
    :initform 0.0)
   (w_right
    :reader w_right
    :initarg :w_right
    :type cl:float
    :initform 0.0)
   (roll
    :reader roll
    :initarg :roll
    :type cl:float
    :initform 0.0)
   (pitch
    :reader pitch
    :initarg :pitch
    :type cl:float
    :initform 0.0)
   (ramp_psi
    :reader ramp_psi
    :initarg :ramp_psi
    :type cl:float
    :initform 0.0)
   (ramp_delta
    :reader ramp_delta
    :initarg :ramp_delta
    :type cl:float
    :initform 0.0))
)

(cl:defclass FullStateSim (<FullStateSim>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FullStateSim>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FullStateSim)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<FullStateSim> is deprecated: use gd_msgs-msg:FullStateSim instead.")))

(cl:ensure-generic-function 'wip_state-val :lambda-list '(m))
(cl:defmethod wip_state-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:wip_state-val is deprecated.  Use gd_msgs-msg:wip_state instead.")
  (wip_state m))

(cl:ensure-generic-function 'desired_state-val :lambda-list '(m))
(cl:defmethod desired_state-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:desired_state-val is deprecated.  Use gd_msgs-msg:desired_state instead.")
  (desired_state m))

(cl:ensure-generic-function 'fdbk_state-val :lambda-list '(m))
(cl:defmethod fdbk_state-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:fdbk_state-val is deprecated.  Use gd_msgs-msg:fdbk_state instead.")
  (fdbk_state m))

(cl:ensure-generic-function 'u_eq_left-val :lambda-list '(m))
(cl:defmethod u_eq_left-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_eq_left-val is deprecated.  Use gd_msgs-msg:u_eq_left instead.")
  (u_eq_left m))

(cl:ensure-generic-function 'u_eq_right-val :lambda-list '(m))
(cl:defmethod u_eq_right-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_eq_right-val is deprecated.  Use gd_msgs-msg:u_eq_right instead.")
  (u_eq_right m))

(cl:ensure-generic-function 'phi_eq-val :lambda-list '(m))
(cl:defmethod phi_eq-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:phi_eq-val is deprecated.  Use gd_msgs-msg:phi_eq instead.")
  (phi_eq m))

(cl:ensure-generic-function 'u_left-val :lambda-list '(m))
(cl:defmethod u_left-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_left-val is deprecated.  Use gd_msgs-msg:u_left instead.")
  (u_left m))

(cl:ensure-generic-function 'u_right-val :lambda-list '(m))
(cl:defmethod u_right-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_right-val is deprecated.  Use gd_msgs-msg:u_right instead.")
  (u_right m))

(cl:ensure-generic-function 'w_left-val :lambda-list '(m))
(cl:defmethod w_left-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:w_left-val is deprecated.  Use gd_msgs-msg:w_left instead.")
  (w_left m))

(cl:ensure-generic-function 'w_right-val :lambda-list '(m))
(cl:defmethod w_right-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:w_right-val is deprecated.  Use gd_msgs-msg:w_right instead.")
  (w_right m))

(cl:ensure-generic-function 'roll-val :lambda-list '(m))
(cl:defmethod roll-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:roll-val is deprecated.  Use gd_msgs-msg:roll instead.")
  (roll m))

(cl:ensure-generic-function 'pitch-val :lambda-list '(m))
(cl:defmethod pitch-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:pitch-val is deprecated.  Use gd_msgs-msg:pitch instead.")
  (pitch m))

(cl:ensure-generic-function 'ramp_psi-val :lambda-list '(m))
(cl:defmethod ramp_psi-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:ramp_psi-val is deprecated.  Use gd_msgs-msg:ramp_psi instead.")
  (ramp_psi m))

(cl:ensure-generic-function 'ramp_delta-val :lambda-list '(m))
(cl:defmethod ramp_delta-val ((m <FullStateSim>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:ramp_delta-val is deprecated.  Use gd_msgs-msg:ramp_delta instead.")
  (ramp_delta m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FullStateSim>) ostream)
  "Serializes a message object of type '<FullStateSim>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'wip_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'desired_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'fdbk_state) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_eq_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_eq_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'phi_eq))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'roll))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'pitch))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ramp_psi))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ramp_delta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FullStateSim>) istream)
  "Deserializes a message object of type '<FullStateSim>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'wip_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'desired_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'fdbk_state) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_eq_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_eq_right) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'phi_eq) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_right) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_right) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'roll) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'pitch) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ramp_psi) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ramp_delta) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FullStateSim>)))
  "Returns string type for a message object of type '<FullStateSim>"
  "gd_msgs/FullStateSim")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FullStateSim)))
  "Returns string type for a message object of type 'FullStateSim"
  "gd_msgs/FullStateSim")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FullStateSim>)))
  "Returns md5sum for a message object of type '<FullStateSim>"
  "8b31cdf4b9e337ab7b2e4b6fe00facc0")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FullStateSim)))
  "Returns md5sum for a message object of type 'FullStateSim"
  "8b31cdf4b9e337ab7b2e4b6fe00facc0")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FullStateSim>)))
  "Returns full string definition for message of type '<FullStateSim>"
  (cl:format cl:nil "# WIP State~%State wip_state~%State desired_state~%State fdbk_state~%~%# Equilibrium condition:~%float32 u_eq_left~%float32 u_eq_right~%float32 phi_eq~%~%# Current actuation:~%float32 u_left~%float32 u_right~%~%# Current wheel velocity:~%float32 w_left~%float32 w_right~%~%# IMU sensed:~%float32 roll~%float32 pitch~%~%# Sensed floor:~%float32 ramp_psi~%float32 ramp_delta~%~%~%================================================================================~%MSG: gd_msgs/State~%# State space vector for balance system~%float32 phi~%float32 dx~%float32 dpsi~%float32 dphi~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FullStateSim)))
  "Returns full string definition for message of type 'FullStateSim"
  (cl:format cl:nil "# WIP State~%State wip_state~%State desired_state~%State fdbk_state~%~%# Equilibrium condition:~%float32 u_eq_left~%float32 u_eq_right~%float32 phi_eq~%~%# Current actuation:~%float32 u_left~%float32 u_right~%~%# Current wheel velocity:~%float32 w_left~%float32 w_right~%~%# IMU sensed:~%float32 roll~%float32 pitch~%~%# Sensed floor:~%float32 ramp_psi~%float32 ramp_delta~%~%~%================================================================================~%MSG: gd_msgs/State~%# State space vector for balance system~%float32 phi~%float32 dx~%float32 dpsi~%float32 dphi~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FullStateSim>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'wip_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'desired_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'fdbk_state))
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FullStateSim>))
  "Converts a ROS message object to a list"
  (cl:list 'FullStateSim
    (cl:cons ':wip_state (wip_state msg))
    (cl:cons ':desired_state (desired_state msg))
    (cl:cons ':fdbk_state (fdbk_state msg))
    (cl:cons ':u_eq_left (u_eq_left msg))
    (cl:cons ':u_eq_right (u_eq_right msg))
    (cl:cons ':phi_eq (phi_eq msg))
    (cl:cons ':u_left (u_left msg))
    (cl:cons ':u_right (u_right msg))
    (cl:cons ':w_left (w_left msg))
    (cl:cons ':w_right (w_right msg))
    (cl:cons ':roll (roll msg))
    (cl:cons ':pitch (pitch msg))
    (cl:cons ':ramp_psi (ramp_psi msg))
    (cl:cons ':ramp_delta (ramp_delta msg))
))
