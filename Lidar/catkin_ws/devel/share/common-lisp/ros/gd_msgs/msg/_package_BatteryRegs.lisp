(cl:in-package gd_msgs-msg)
(cl:export '(CONNECTED-VAL
          CONNECTED
          TEMPERATURE-VAL
          TEMPERATURE
          VOLTAGE-VAL
          VOLTAGE
          CURRENT-VAL
          CURRENT
          REMAINING_CAPACITY-VAL
          REMAINING_CAPACITY
          CYCLES_COUNT-VAL
          CYCLES_COUNT
))