; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude FullStateVel.msg.html

(cl:defclass <FullStateVel> (roslisp-msg-protocol:ros-message)
  ((wip_state
    :reader wip_state
    :initarg :wip_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (desired_state
    :reader desired_state
    :initarg :desired_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (fdbk_state
    :reader fdbk_state
    :initarg :fdbk_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (u_left
    :reader u_left
    :initarg :u_left
    :type cl:float
    :initform 0.0)
   (u_right
    :reader u_right
    :initarg :u_right
    :type cl:float
    :initform 0.0)
   (w_fbk_left
    :reader w_fbk_left
    :initarg :w_fbk_left
    :type cl:float
    :initform 0.0)
   (w_fbk_right
    :reader w_fbk_right
    :initarg :w_fbk_right
    :type cl:float
    :initform 0.0)
   (w_cmd_left
    :reader w_cmd_left
    :initarg :w_cmd_left
    :type cl:float
    :initform 0.0)
   (w_cmd_right
    :reader w_cmd_right
    :initarg :w_cmd_right
    :type cl:float
    :initform 0.0))
)

(cl:defclass FullStateVel (<FullStateVel>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FullStateVel>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FullStateVel)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<FullStateVel> is deprecated: use gd_msgs-msg:FullStateVel instead.")))

(cl:ensure-generic-function 'wip_state-val :lambda-list '(m))
(cl:defmethod wip_state-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:wip_state-val is deprecated.  Use gd_msgs-msg:wip_state instead.")
  (wip_state m))

(cl:ensure-generic-function 'desired_state-val :lambda-list '(m))
(cl:defmethod desired_state-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:desired_state-val is deprecated.  Use gd_msgs-msg:desired_state instead.")
  (desired_state m))

(cl:ensure-generic-function 'fdbk_state-val :lambda-list '(m))
(cl:defmethod fdbk_state-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:fdbk_state-val is deprecated.  Use gd_msgs-msg:fdbk_state instead.")
  (fdbk_state m))

(cl:ensure-generic-function 'u_left-val :lambda-list '(m))
(cl:defmethod u_left-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_left-val is deprecated.  Use gd_msgs-msg:u_left instead.")
  (u_left m))

(cl:ensure-generic-function 'u_right-val :lambda-list '(m))
(cl:defmethod u_right-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_right-val is deprecated.  Use gd_msgs-msg:u_right instead.")
  (u_right m))

(cl:ensure-generic-function 'w_fbk_left-val :lambda-list '(m))
(cl:defmethod w_fbk_left-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:w_fbk_left-val is deprecated.  Use gd_msgs-msg:w_fbk_left instead.")
  (w_fbk_left m))

(cl:ensure-generic-function 'w_fbk_right-val :lambda-list '(m))
(cl:defmethod w_fbk_right-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:w_fbk_right-val is deprecated.  Use gd_msgs-msg:w_fbk_right instead.")
  (w_fbk_right m))

(cl:ensure-generic-function 'w_cmd_left-val :lambda-list '(m))
(cl:defmethod w_cmd_left-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:w_cmd_left-val is deprecated.  Use gd_msgs-msg:w_cmd_left instead.")
  (w_cmd_left m))

(cl:ensure-generic-function 'w_cmd_right-val :lambda-list '(m))
(cl:defmethod w_cmd_right-val ((m <FullStateVel>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:w_cmd_right-val is deprecated.  Use gd_msgs-msg:w_cmd_right instead.")
  (w_cmd_right m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FullStateVel>) ostream)
  "Serializes a message object of type '<FullStateVel>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'wip_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'desired_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'fdbk_state) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_fbk_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_fbk_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_cmd_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'w_cmd_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FullStateVel>) istream)
  "Deserializes a message object of type '<FullStateVel>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'wip_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'desired_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'fdbk_state) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_right) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_fbk_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_fbk_right) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_cmd_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'w_cmd_right) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FullStateVel>)))
  "Returns string type for a message object of type '<FullStateVel>"
  "gd_msgs/FullStateVel")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FullStateVel)))
  "Returns string type for a message object of type 'FullStateVel"
  "gd_msgs/FullStateVel")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FullStateVel>)))
  "Returns md5sum for a message object of type '<FullStateVel>"
  "bf812aae0ef8e4ee99c189ac78321047")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FullStateVel)))
  "Returns md5sum for a message object of type 'FullStateVel"
  "bf812aae0ef8e4ee99c189ac78321047")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FullStateVel>)))
  "Returns full string definition for message of type '<FullStateVel>"
  (cl:format cl:nil "# WIP State~%State wip_state~%State desired_state~%State fdbk_state~%~%# Current actuation:~%float32 u_left~%float32 u_right~%~%# Current wheel velocity:~%float32 w_fbk_left~%float32 w_fbk_right~%~%# Wheel command:~%float32 w_cmd_left~%float32 w_cmd_right~%~%================================================================================~%MSG: gd_msgs/State~%# State space vector for balance system~%float32 phi~%float32 dx~%float32 dpsi~%float32 dphi~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FullStateVel)))
  "Returns full string definition for message of type 'FullStateVel"
  (cl:format cl:nil "# WIP State~%State wip_state~%State desired_state~%State fdbk_state~%~%# Current actuation:~%float32 u_left~%float32 u_right~%~%# Current wheel velocity:~%float32 w_fbk_left~%float32 w_fbk_right~%~%# Wheel command:~%float32 w_cmd_left~%float32 w_cmd_right~%~%================================================================================~%MSG: gd_msgs/State~%# State space vector for balance system~%float32 phi~%float32 dx~%float32 dpsi~%float32 dphi~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FullStateVel>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'wip_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'desired_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'fdbk_state))
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FullStateVel>))
  "Converts a ROS message object to a list"
  (cl:list 'FullStateVel
    (cl:cons ':wip_state (wip_state msg))
    (cl:cons ':desired_state (desired_state msg))
    (cl:cons ':fdbk_state (fdbk_state msg))
    (cl:cons ':u_left (u_left msg))
    (cl:cons ':u_right (u_right msg))
    (cl:cons ':w_fbk_left (w_fbk_left msg))
    (cl:cons ':w_fbk_right (w_fbk_right msg))
    (cl:cons ':w_cmd_left (w_cmd_left msg))
    (cl:cons ':w_cmd_right (w_cmd_right msg))
))
