(cl:in-package gd_msgs-msg)
(cl:export '(POSITION-VAL
          POSITION
          VELOCITY-VAL
          VELOCITY
          K-VAL
          K
          KI-VAL
          KI
          KD-VAL
          KD
))