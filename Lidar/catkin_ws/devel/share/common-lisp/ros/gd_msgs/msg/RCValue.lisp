; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude RCValue.msg.html

(cl:defclass <RCValue> (roslisp-msg-protocol:ros-message)
  ((connected
    :reader connected
    :initarg :connected
    :type cl:boolean
    :initform cl:nil)
   (value
    :reader value
    :initarg :value
    :type cl:fixnum
    :initform 0))
)

(cl:defclass RCValue (<RCValue>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RCValue>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RCValue)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<RCValue> is deprecated: use gd_msgs-msg:RCValue instead.")))

(cl:ensure-generic-function 'connected-val :lambda-list '(m))
(cl:defmethod connected-val ((m <RCValue>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:connected-val is deprecated.  Use gd_msgs-msg:connected instead.")
  (connected m))

(cl:ensure-generic-function 'value-val :lambda-list '(m))
(cl:defmethod value-val ((m <RCValue>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:value-val is deprecated.  Use gd_msgs-msg:value instead.")
  (value m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RCValue>) ostream)
  "Serializes a message object of type '<RCValue>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'connected) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'value)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RCValue>) istream)
  "Deserializes a message object of type '<RCValue>"
    (cl:setf (cl:slot-value msg 'connected) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'value) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RCValue>)))
  "Returns string type for a message object of type '<RCValue>"
  "gd_msgs/RCValue")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RCValue)))
  "Returns string type for a message object of type 'RCValue"
  "gd_msgs/RCValue")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RCValue>)))
  "Returns md5sum for a message object of type '<RCValue>"
  "f8111cd0d17d8f3a681cc1d3196c2d07")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RCValue)))
  "Returns md5sum for a message object of type 'RCValue"
  "f8111cd0d17d8f3a681cc1d3196c2d07")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RCValue>)))
  "Returns full string definition for message of type '<RCValue>"
  (cl:format cl:nil "bool connected~%int16 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RCValue)))
  "Returns full string definition for message of type 'RCValue"
  (cl:format cl:nil "bool connected~%int16 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RCValue>))
  (cl:+ 0
     1
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RCValue>))
  "Converts a ROS message object to a list"
  (cl:list 'RCValue
    (cl:cons ':connected (connected msg))
    (cl:cons ':value (value msg))
))
