; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude BatteryRegs.msg.html

(cl:defclass <BatteryRegs> (roslisp-msg-protocol:ros-message)
  ((connected
    :reader connected
    :initarg :connected
    :type cl:boolean
    :initform cl:nil)
   (temperature
    :reader temperature
    :initarg :temperature
    :type cl:fixnum
    :initform 0)
   (voltage
    :reader voltage
    :initarg :voltage
    :type cl:fixnum
    :initform 0)
   (current
    :reader current
    :initarg :current
    :type cl:fixnum
    :initform 0)
   (remaining_capacity
    :reader remaining_capacity
    :initarg :remaining_capacity
    :type cl:fixnum
    :initform 0)
   (cycles_count
    :reader cycles_count
    :initarg :cycles_count
    :type cl:fixnum
    :initform 0))
)

(cl:defclass BatteryRegs (<BatteryRegs>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BatteryRegs>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BatteryRegs)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<BatteryRegs> is deprecated: use gd_msgs-msg:BatteryRegs instead.")))

(cl:ensure-generic-function 'connected-val :lambda-list '(m))
(cl:defmethod connected-val ((m <BatteryRegs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:connected-val is deprecated.  Use gd_msgs-msg:connected instead.")
  (connected m))

(cl:ensure-generic-function 'temperature-val :lambda-list '(m))
(cl:defmethod temperature-val ((m <BatteryRegs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:temperature-val is deprecated.  Use gd_msgs-msg:temperature instead.")
  (temperature m))

(cl:ensure-generic-function 'voltage-val :lambda-list '(m))
(cl:defmethod voltage-val ((m <BatteryRegs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:voltage-val is deprecated.  Use gd_msgs-msg:voltage instead.")
  (voltage m))

(cl:ensure-generic-function 'current-val :lambda-list '(m))
(cl:defmethod current-val ((m <BatteryRegs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:current-val is deprecated.  Use gd_msgs-msg:current instead.")
  (current m))

(cl:ensure-generic-function 'remaining_capacity-val :lambda-list '(m))
(cl:defmethod remaining_capacity-val ((m <BatteryRegs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:remaining_capacity-val is deprecated.  Use gd_msgs-msg:remaining_capacity instead.")
  (remaining_capacity m))

(cl:ensure-generic-function 'cycles_count-val :lambda-list '(m))
(cl:defmethod cycles_count-val ((m <BatteryRegs>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:cycles_count-val is deprecated.  Use gd_msgs-msg:cycles_count instead.")
  (cycles_count m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BatteryRegs>) ostream)
  "Serializes a message object of type '<BatteryRegs>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'connected) 1 0)) ostream)
  (cl:let* ((signed (cl:slot-value msg 'temperature)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'voltage)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'current)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'remaining_capacity)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'cycles_count)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 65536) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BatteryRegs>) istream)
  "Deserializes a message object of type '<BatteryRegs>"
    (cl:setf (cl:slot-value msg 'connected) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'temperature) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'voltage) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'current) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'remaining_capacity) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'cycles_count) (cl:if (cl:< unsigned 32768) unsigned (cl:- unsigned 65536))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BatteryRegs>)))
  "Returns string type for a message object of type '<BatteryRegs>"
  "gd_msgs/BatteryRegs")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BatteryRegs)))
  "Returns string type for a message object of type 'BatteryRegs"
  "gd_msgs/BatteryRegs")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BatteryRegs>)))
  "Returns md5sum for a message object of type '<BatteryRegs>"
  "823d1929afb53aaad11fd102584b3f5f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BatteryRegs)))
  "Returns md5sum for a message object of type 'BatteryRegs"
  "823d1929afb53aaad11fd102584b3f5f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BatteryRegs>)))
  "Returns full string definition for message of type '<BatteryRegs>"
  (cl:format cl:nil "bool connected~%int16 temperature~%int16 voltage~%int16 current~%int16 remaining_capacity~%int16 cycles_count~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BatteryRegs)))
  "Returns full string definition for message of type 'BatteryRegs"
  (cl:format cl:nil "bool connected~%int16 temperature~%int16 voltage~%int16 current~%int16 remaining_capacity~%int16 cycles_count~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BatteryRegs>))
  (cl:+ 0
     1
     2
     2
     2
     2
     2
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BatteryRegs>))
  "Converts a ROS message object to a list"
  (cl:list 'BatteryRegs
    (cl:cons ':connected (connected msg))
    (cl:cons ':temperature (temperature msg))
    (cl:cons ':voltage (voltage msg))
    (cl:cons ':current (current msg))
    (cl:cons ':remaining_capacity (remaining_capacity msg))
    (cl:cons ':cycles_count (cycles_count msg))
))
