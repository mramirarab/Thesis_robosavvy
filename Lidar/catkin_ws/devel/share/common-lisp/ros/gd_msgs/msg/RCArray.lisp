; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude RCArray.msg.html

(cl:defclass <RCArray> (roslisp-msg-protocol:ros-message)
  ((channel
    :reader channel
    :initarg :channel
    :type (cl:vector gd_msgs-msg:RCValue)
   :initform (cl:make-array 14 :element-type 'gd_msgs-msg:RCValue :initial-element (cl:make-instance 'gd_msgs-msg:RCValue))))
)

(cl:defclass RCArray (<RCArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RCArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RCArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<RCArray> is deprecated: use gd_msgs-msg:RCArray instead.")))

(cl:ensure-generic-function 'channel-val :lambda-list '(m))
(cl:defmethod channel-val ((m <RCArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:channel-val is deprecated.  Use gd_msgs-msg:channel instead.")
  (channel m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RCArray>) ostream)
  "Serializes a message object of type '<RCArray>"
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'channel))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RCArray>) istream)
  "Deserializes a message object of type '<RCArray>"
  (cl:setf (cl:slot-value msg 'channel) (cl:make-array 14))
  (cl:let ((vals (cl:slot-value msg 'channel)))
    (cl:dotimes (i 14)
    (cl:setf (cl:aref vals i) (cl:make-instance 'gd_msgs-msg:RCValue))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RCArray>)))
  "Returns string type for a message object of type '<RCArray>"
  "gd_msgs/RCArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RCArray)))
  "Returns string type for a message object of type 'RCArray"
  "gd_msgs/RCArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RCArray>)))
  "Returns md5sum for a message object of type '<RCArray>"
  "ed6b509efd13af7cb1c101e3d45fe032")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RCArray)))
  "Returns md5sum for a message object of type 'RCArray"
  "ed6b509efd13af7cb1c101e3d45fe032")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RCArray>)))
  "Returns full string definition for message of type '<RCArray>"
  (cl:format cl:nil "RCValue[14] channel~%~%================================================================================~%MSG: gd_msgs/RCValue~%bool connected~%int16 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RCArray)))
  "Returns full string definition for message of type 'RCArray"
  (cl:format cl:nil "RCValue[14] channel~%~%================================================================================~%MSG: gd_msgs/RCValue~%bool connected~%int16 value~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RCArray>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'channel) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RCArray>))
  "Converts a ROS message object to a list"
  (cl:list 'RCArray
    (cl:cons ':channel (channel msg))
))
