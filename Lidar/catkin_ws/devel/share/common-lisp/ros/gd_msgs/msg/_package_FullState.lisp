(cl:in-package gd_msgs-msg)
(cl:export '(WIP_STATE-VAL
          WIP_STATE
          DESIRED_STATE-VAL
          DESIRED_STATE
          FDBK_STATE-VAL
          FDBK_STATE
          U_LEFT-VAL
          U_LEFT
          U_RIGHT-VAL
          U_RIGHT
))