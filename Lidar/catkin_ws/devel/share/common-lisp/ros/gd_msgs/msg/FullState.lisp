; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude FullState.msg.html

(cl:defclass <FullState> (roslisp-msg-protocol:ros-message)
  ((wip_state
    :reader wip_state
    :initarg :wip_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (desired_state
    :reader desired_state
    :initarg :desired_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (fdbk_state
    :reader fdbk_state
    :initarg :fdbk_state
    :type gd_msgs-msg:State
    :initform (cl:make-instance 'gd_msgs-msg:State))
   (u_left
    :reader u_left
    :initarg :u_left
    :type cl:float
    :initform 0.0)
   (u_right
    :reader u_right
    :initarg :u_right
    :type cl:float
    :initform 0.0))
)

(cl:defclass FullState (<FullState>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <FullState>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'FullState)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<FullState> is deprecated: use gd_msgs-msg:FullState instead.")))

(cl:ensure-generic-function 'wip_state-val :lambda-list '(m))
(cl:defmethod wip_state-val ((m <FullState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:wip_state-val is deprecated.  Use gd_msgs-msg:wip_state instead.")
  (wip_state m))

(cl:ensure-generic-function 'desired_state-val :lambda-list '(m))
(cl:defmethod desired_state-val ((m <FullState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:desired_state-val is deprecated.  Use gd_msgs-msg:desired_state instead.")
  (desired_state m))

(cl:ensure-generic-function 'fdbk_state-val :lambda-list '(m))
(cl:defmethod fdbk_state-val ((m <FullState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:fdbk_state-val is deprecated.  Use gd_msgs-msg:fdbk_state instead.")
  (fdbk_state m))

(cl:ensure-generic-function 'u_left-val :lambda-list '(m))
(cl:defmethod u_left-val ((m <FullState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_left-val is deprecated.  Use gd_msgs-msg:u_left instead.")
  (u_left m))

(cl:ensure-generic-function 'u_right-val :lambda-list '(m))
(cl:defmethod u_right-val ((m <FullState>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:u_right-val is deprecated.  Use gd_msgs-msg:u_right instead.")
  (u_right m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <FullState>) ostream)
  "Serializes a message object of type '<FullState>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'wip_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'desired_state) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'fdbk_state) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_left))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'u_right))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <FullState>) istream)
  "Deserializes a message object of type '<FullState>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'wip_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'desired_state) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'fdbk_state) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_left) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'u_right) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<FullState>)))
  "Returns string type for a message object of type '<FullState>"
  "gd_msgs/FullState")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'FullState)))
  "Returns string type for a message object of type 'FullState"
  "gd_msgs/FullState")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<FullState>)))
  "Returns md5sum for a message object of type '<FullState>"
  "c813ba5aa7cf30286a706a0a3e0582cb")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'FullState)))
  "Returns md5sum for a message object of type 'FullState"
  "c813ba5aa7cf30286a706a0a3e0582cb")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<FullState>)))
  "Returns full string definition for message of type '<FullState>"
  (cl:format cl:nil "# WIP State~%State wip_state~%State desired_state~%State fdbk_state~%~%# Current actuation:~%float32 u_left~%float32 u_right~%~%================================================================================~%MSG: gd_msgs/State~%# State space vector for balance system~%float32 phi~%float32 dx~%float32 dpsi~%float32 dphi~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'FullState)))
  "Returns full string definition for message of type 'FullState"
  (cl:format cl:nil "# WIP State~%State wip_state~%State desired_state~%State fdbk_state~%~%# Current actuation:~%float32 u_left~%float32 u_right~%~%================================================================================~%MSG: gd_msgs/State~%# State space vector for balance system~%float32 phi~%float32 dx~%float32 dpsi~%float32 dphi~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <FullState>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'wip_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'desired_state))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'fdbk_state))
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <FullState>))
  "Converts a ROS message object to a list"
  (cl:list 'FullState
    (cl:cons ':wip_state (wip_state msg))
    (cl:cons ':desired_state (desired_state msg))
    (cl:cons ':fdbk_state (fdbk_state msg))
    (cl:cons ':u_left (u_left msg))
    (cl:cons ':u_right (u_right msg))
))
