; Auto-generated. Do not edit!


(cl:in-package gd_msgs-msg)


;//! \htmlinclude BatteryRegsArray.msg.html

(cl:defclass <BatteryRegsArray> (roslisp-msg-protocol:ros-message)
  ((battery
    :reader battery
    :initarg :battery
    :type (cl:vector gd_msgs-msg:BatteryRegs)
   :initform (cl:make-array 2 :element-type 'gd_msgs-msg:BatteryRegs :initial-element (cl:make-instance 'gd_msgs-msg:BatteryRegs))))
)

(cl:defclass BatteryRegsArray (<BatteryRegsArray>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <BatteryRegsArray>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'BatteryRegsArray)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-msg:<BatteryRegsArray> is deprecated: use gd_msgs-msg:BatteryRegsArray instead.")))

(cl:ensure-generic-function 'battery-val :lambda-list '(m))
(cl:defmethod battery-val ((m <BatteryRegsArray>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-msg:battery-val is deprecated.  Use gd_msgs-msg:battery instead.")
  (battery m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <BatteryRegsArray>) ostream)
  "Serializes a message object of type '<BatteryRegsArray>"
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'battery))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <BatteryRegsArray>) istream)
  "Deserializes a message object of type '<BatteryRegsArray>"
  (cl:setf (cl:slot-value msg 'battery) (cl:make-array 2))
  (cl:let ((vals (cl:slot-value msg 'battery)))
    (cl:dotimes (i 2)
    (cl:setf (cl:aref vals i) (cl:make-instance 'gd_msgs-msg:BatteryRegs))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<BatteryRegsArray>)))
  "Returns string type for a message object of type '<BatteryRegsArray>"
  "gd_msgs/BatteryRegsArray")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'BatteryRegsArray)))
  "Returns string type for a message object of type 'BatteryRegsArray"
  "gd_msgs/BatteryRegsArray")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<BatteryRegsArray>)))
  "Returns md5sum for a message object of type '<BatteryRegsArray>"
  "088a6a0e1bf795cb18e5c5f436e1d837")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'BatteryRegsArray)))
  "Returns md5sum for a message object of type 'BatteryRegsArray"
  "088a6a0e1bf795cb18e5c5f436e1d837")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<BatteryRegsArray>)))
  "Returns full string definition for message of type '<BatteryRegsArray>"
  (cl:format cl:nil "BatteryRegs[2] battery~%~%================================================================================~%MSG: gd_msgs/BatteryRegs~%bool connected~%int16 temperature~%int16 voltage~%int16 current~%int16 remaining_capacity~%int16 cycles_count~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'BatteryRegsArray)))
  "Returns full string definition for message of type 'BatteryRegsArray"
  (cl:format cl:nil "BatteryRegs[2] battery~%~%================================================================================~%MSG: gd_msgs/BatteryRegs~%bool connected~%int16 temperature~%int16 voltage~%int16 current~%int16 remaining_capacity~%int16 cycles_count~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <BatteryRegsArray>))
  (cl:+ 0
     0 (cl:reduce #'cl:+ (cl:slot-value msg 'battery) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <BatteryRegsArray>))
  "Converts a ROS message object to a list"
  (cl:list 'BatteryRegsArray
    (cl:cons ':battery (battery msg))
))
