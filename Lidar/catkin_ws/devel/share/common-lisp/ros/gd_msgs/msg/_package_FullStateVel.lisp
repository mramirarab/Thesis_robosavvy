(cl:in-package gd_msgs-msg)
(cl:export '(WIP_STATE-VAL
          WIP_STATE
          DESIRED_STATE-VAL
          DESIRED_STATE
          FDBK_STATE-VAL
          FDBK_STATE
          U_LEFT-VAL
          U_LEFT
          U_RIGHT-VAL
          U_RIGHT
          W_FBK_LEFT-VAL
          W_FBK_LEFT
          W_FBK_RIGHT-VAL
          W_FBK_RIGHT
          W_CMD_LEFT-VAL
          W_CMD_LEFT
          W_CMD_RIGHT-VAL
          W_CMD_RIGHT
))