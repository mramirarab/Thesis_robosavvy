
(cl:in-package :asdf)

(defsystem "gd_msgs-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "SetInput" :depends-on ("_package_SetInput"))
    (:file "_package_SetInput" :depends-on ("_package"))
    (:file "SetMode" :depends-on ("_package_SetMode"))
    (:file "_package_SetMode" :depends-on ("_package"))
  ))