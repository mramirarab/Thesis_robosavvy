; Auto-generated. Do not edit!


(cl:in-package gd_msgs-srv)


;//! \htmlinclude SetInput-request.msg.html

(cl:defclass <SetInput-request> (roslisp-msg-protocol:ros-message)
  ((input
    :reader input
    :initarg :input
    :type cl:integer
    :initform 0))
)

(cl:defclass SetInput-request (<SetInput-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetInput-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetInput-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-srv:<SetInput-request> is deprecated: use gd_msgs-srv:SetInput-request instead.")))

(cl:ensure-generic-function 'input-val :lambda-list '(m))
(cl:defmethod input-val ((m <SetInput-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader gd_msgs-srv:input-val is deprecated.  Use gd_msgs-srv:input instead.")
  (input m))
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql '<SetInput-request>)))
    "Constants for message type '<SetInput-request>"
  '((:PC . 0)
    (:RC . 1)
    (:MIX . 2))
)
(cl:defmethod roslisp-msg-protocol:symbol-codes ((msg-type (cl:eql 'SetInput-request)))
    "Constants for message type 'SetInput-request"
  '((:PC . 0)
    (:RC . 1)
    (:MIX . 2))
)
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetInput-request>) ostream)
  "Serializes a message object of type '<SetInput-request>"
  (cl:let* ((signed (cl:slot-value msg 'input)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetInput-request>) istream)
  "Deserializes a message object of type '<SetInput-request>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'input) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetInput-request>)))
  "Returns string type for a service object of type '<SetInput-request>"
  "gd_msgs/SetInputRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetInput-request)))
  "Returns string type for a service object of type 'SetInput-request"
  "gd_msgs/SetInputRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetInput-request>)))
  "Returns md5sum for a message object of type '<SetInput-request>"
  "d624d0a06e7d9cd337fb3219e1a659d5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetInput-request)))
  "Returns md5sum for a message object of type 'SetInput-request"
  "d624d0a06e7d9cd337fb3219e1a659d5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetInput-request>)))
  "Returns full string definition for message of type '<SetInput-request>"
  (cl:format cl:nil "~%~%~%~%~%~%~%int32 PC = 0~%int32 RC = 1~%int32 MIX = 2~%~%~%int32 input~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetInput-request)))
  "Returns full string definition for message of type 'SetInput-request"
  (cl:format cl:nil "~%~%~%~%~%~%~%int32 PC = 0~%int32 RC = 1~%int32 MIX = 2~%~%~%int32 input~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetInput-request>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetInput-request>))
  "Converts a ROS message object to a list"
  (cl:list 'SetInput-request
    (cl:cons ':input (input msg))
))
;//! \htmlinclude SetInput-response.msg.html

(cl:defclass <SetInput-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass SetInput-response (<SetInput-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <SetInput-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'SetInput-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name gd_msgs-srv:<SetInput-response> is deprecated: use gd_msgs-srv:SetInput-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <SetInput-response>) ostream)
  "Serializes a message object of type '<SetInput-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <SetInput-response>) istream)
  "Deserializes a message object of type '<SetInput-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<SetInput-response>)))
  "Returns string type for a service object of type '<SetInput-response>"
  "gd_msgs/SetInputResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetInput-response)))
  "Returns string type for a service object of type 'SetInput-response"
  "gd_msgs/SetInputResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<SetInput-response>)))
  "Returns md5sum for a message object of type '<SetInput-response>"
  "d624d0a06e7d9cd337fb3219e1a659d5")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'SetInput-response)))
  "Returns md5sum for a message object of type 'SetInput-response"
  "d624d0a06e7d9cd337fb3219e1a659d5")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<SetInput-response>)))
  "Returns full string definition for message of type '<SetInput-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'SetInput-response)))
  "Returns full string definition for message of type 'SetInput-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <SetInput-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <SetInput-response>))
  "Converts a ROS message object to a list"
  (cl:list 'SetInput-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'SetInput)))
  'SetInput-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'SetInput)))
  'SetInput-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'SetInput)))
  "Returns string type for a service object of type '<SetInput>"
  "gd_msgs/SetInput")