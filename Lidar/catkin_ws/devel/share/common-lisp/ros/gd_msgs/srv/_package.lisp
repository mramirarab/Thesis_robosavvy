(cl:defpackage gd_msgs-srv
  (:use )
  (:export
   "SETINPUT"
   "<SETINPUT-REQUEST>"
   "SETINPUT-REQUEST"
   "<SETINPUT-RESPONSE>"
   "SETINPUT-RESPONSE"
   "SETMODE"
   "<SETMODE-REQUEST>"
   "SETMODE-REQUEST"
   "<SETMODE-RESPONSE>"
   "SETMODE-RESPONSE"
  ))

