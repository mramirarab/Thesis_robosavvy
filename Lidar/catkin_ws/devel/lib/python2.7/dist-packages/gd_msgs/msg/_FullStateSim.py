# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from gd_msgs/FullStateSim.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import gd_msgs.msg

class FullStateSim(genpy.Message):
  _md5sum = "8b31cdf4b9e337ab7b2e4b6fe00facc0"
  _type = "gd_msgs/FullStateSim"
  _has_header = False #flag to mark the presence of a Header object
  _full_text = """# WIP State
State wip_state
State desired_state
State fdbk_state

# Equilibrium condition:
float32 u_eq_left
float32 u_eq_right
float32 phi_eq

# Current actuation:
float32 u_left
float32 u_right

# Current wheel velocity:
float32 w_left
float32 w_right

# IMU sensed:
float32 roll
float32 pitch

# Sensed floor:
float32 ramp_psi
float32 ramp_delta


================================================================================
MSG: gd_msgs/State
# State space vector for balance system
float32 phi
float32 dx
float32 dpsi
float32 dphi
"""
  __slots__ = ['wip_state','desired_state','fdbk_state','u_eq_left','u_eq_right','phi_eq','u_left','u_right','w_left','w_right','roll','pitch','ramp_psi','ramp_delta']
  _slot_types = ['gd_msgs/State','gd_msgs/State','gd_msgs/State','float32','float32','float32','float32','float32','float32','float32','float32','float32','float32','float32']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       wip_state,desired_state,fdbk_state,u_eq_left,u_eq_right,phi_eq,u_left,u_right,w_left,w_right,roll,pitch,ramp_psi,ramp_delta

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(FullStateSim, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.wip_state is None:
        self.wip_state = gd_msgs.msg.State()
      if self.desired_state is None:
        self.desired_state = gd_msgs.msg.State()
      if self.fdbk_state is None:
        self.fdbk_state = gd_msgs.msg.State()
      if self.u_eq_left is None:
        self.u_eq_left = 0.
      if self.u_eq_right is None:
        self.u_eq_right = 0.
      if self.phi_eq is None:
        self.phi_eq = 0.
      if self.u_left is None:
        self.u_left = 0.
      if self.u_right is None:
        self.u_right = 0.
      if self.w_left is None:
        self.w_left = 0.
      if self.w_right is None:
        self.w_right = 0.
      if self.roll is None:
        self.roll = 0.
      if self.pitch is None:
        self.pitch = 0.
      if self.ramp_psi is None:
        self.ramp_psi = 0.
      if self.ramp_delta is None:
        self.ramp_delta = 0.
    else:
      self.wip_state = gd_msgs.msg.State()
      self.desired_state = gd_msgs.msg.State()
      self.fdbk_state = gd_msgs.msg.State()
      self.u_eq_left = 0.
      self.u_eq_right = 0.
      self.phi_eq = 0.
      self.u_left = 0.
      self.u_right = 0.
      self.w_left = 0.
      self.w_right = 0.
      self.roll = 0.
      self.pitch = 0.
      self.ramp_psi = 0.
      self.ramp_delta = 0.

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_get_struct_23f().pack(_x.wip_state.phi, _x.wip_state.dx, _x.wip_state.dpsi, _x.wip_state.dphi, _x.desired_state.phi, _x.desired_state.dx, _x.desired_state.dpsi, _x.desired_state.dphi, _x.fdbk_state.phi, _x.fdbk_state.dx, _x.fdbk_state.dpsi, _x.fdbk_state.dphi, _x.u_eq_left, _x.u_eq_right, _x.phi_eq, _x.u_left, _x.u_right, _x.w_left, _x.w_right, _x.roll, _x.pitch, _x.ramp_psi, _x.ramp_delta))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.wip_state is None:
        self.wip_state = gd_msgs.msg.State()
      if self.desired_state is None:
        self.desired_state = gd_msgs.msg.State()
      if self.fdbk_state is None:
        self.fdbk_state = gd_msgs.msg.State()
      end = 0
      _x = self
      start = end
      end += 92
      (_x.wip_state.phi, _x.wip_state.dx, _x.wip_state.dpsi, _x.wip_state.dphi, _x.desired_state.phi, _x.desired_state.dx, _x.desired_state.dpsi, _x.desired_state.dphi, _x.fdbk_state.phi, _x.fdbk_state.dx, _x.fdbk_state.dpsi, _x.fdbk_state.dphi, _x.u_eq_left, _x.u_eq_right, _x.phi_eq, _x.u_left, _x.u_right, _x.w_left, _x.w_right, _x.roll, _x.pitch, _x.ramp_psi, _x.ramp_delta,) = _get_struct_23f().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_get_struct_23f().pack(_x.wip_state.phi, _x.wip_state.dx, _x.wip_state.dpsi, _x.wip_state.dphi, _x.desired_state.phi, _x.desired_state.dx, _x.desired_state.dpsi, _x.desired_state.dphi, _x.fdbk_state.phi, _x.fdbk_state.dx, _x.fdbk_state.dpsi, _x.fdbk_state.dphi, _x.u_eq_left, _x.u_eq_right, _x.phi_eq, _x.u_left, _x.u_right, _x.w_left, _x.w_right, _x.roll, _x.pitch, _x.ramp_psi, _x.ramp_delta))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.wip_state is None:
        self.wip_state = gd_msgs.msg.State()
      if self.desired_state is None:
        self.desired_state = gd_msgs.msg.State()
      if self.fdbk_state is None:
        self.fdbk_state = gd_msgs.msg.State()
      end = 0
      _x = self
      start = end
      end += 92
      (_x.wip_state.phi, _x.wip_state.dx, _x.wip_state.dpsi, _x.wip_state.dphi, _x.desired_state.phi, _x.desired_state.dx, _x.desired_state.dpsi, _x.desired_state.dphi, _x.fdbk_state.phi, _x.fdbk_state.dx, _x.fdbk_state.dpsi, _x.fdbk_state.dphi, _x.u_eq_left, _x.u_eq_right, _x.phi_eq, _x.u_left, _x.u_right, _x.w_left, _x.w_right, _x.roll, _x.pitch, _x.ramp_psi, _x.ramp_delta,) = _get_struct_23f().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
def _get_struct_I():
    global _struct_I
    return _struct_I
_struct_23f = None
def _get_struct_23f():
    global _struct_23f
    if _struct_23f is None:
        _struct_23f = struct.Struct("<23f")
    return _struct_23f
