# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "gd_msgs: 10 messages, 2 services")

set(MSG_I_FLAGS "-Igd_msgs:/home/gil/catkin_ws/src/gd_msgs/msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(gd_msgs_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" "gd_msgs/State"
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" "gd_msgs/State"
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" ""
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" "gd_msgs/State"
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" "gd_msgs/BatteryRegs"
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" ""
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" ""
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" ""
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" "gd_msgs/RCValue"
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" "gd_msgs/State"
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" ""
)

get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" NAME_WE)
add_custom_target(_gd_msgs_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gd_msgs" "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_msg_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)

### Generating Services
_generate_srv_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)
_generate_srv_cpp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
)

### Generating Module File
_generate_module_cpp(gd_msgs
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(gd_msgs_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(gd_msgs_generate_messages gd_msgs_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_cpp _gd_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gd_msgs_gencpp)
add_dependencies(gd_msgs_gencpp gd_msgs_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gd_msgs_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_msg_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)

### Generating Services
_generate_srv_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)
_generate_srv_eus(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
)

### Generating Module File
_generate_module_eus(gd_msgs
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(gd_msgs_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(gd_msgs_generate_messages gd_msgs_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_eus _gd_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gd_msgs_geneus)
add_dependencies(gd_msgs_geneus gd_msgs_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gd_msgs_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_msg_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)

### Generating Services
_generate_srv_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)
_generate_srv_lisp(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
)

### Generating Module File
_generate_module_lisp(gd_msgs
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(gd_msgs_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(gd_msgs_generate_messages gd_msgs_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_lisp _gd_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gd_msgs_genlisp)
add_dependencies(gd_msgs_genlisp gd_msgs_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gd_msgs_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_msg_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)

### Generating Services
_generate_srv_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)
_generate_srv_nodejs(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
)

### Generating Module File
_generate_module_nodejs(gd_msgs
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(gd_msgs_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(gd_msgs_generate_messages gd_msgs_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_nodejs _gd_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gd_msgs_gennodejs)
add_dependencies(gd_msgs_gennodejs gd_msgs_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gd_msgs_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_msg_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg"
  "${MSG_I_FLAGS}"
  "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)

### Generating Services
_generate_srv_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)
_generate_srv_py(gd_msgs
  "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
)

### Generating Module File
_generate_module_py(gd_msgs
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(gd_msgs_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(gd_msgs_generate_messages gd_msgs_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv" NAME_WE)
add_dependencies(gd_msgs_generate_messages_py _gd_msgs_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gd_msgs_genpy)
add_dependencies(gd_msgs_genpy gd_msgs_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gd_msgs_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gd_msgs
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(gd_msgs_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/gd_msgs
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(gd_msgs_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gd_msgs
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(gd_msgs_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/gd_msgs
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(gd_msgs_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gd_msgs
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(gd_msgs_generate_messages_py std_msgs_generate_messages_py)
endif()
