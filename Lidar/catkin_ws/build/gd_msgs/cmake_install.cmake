# Install script for directory: /home/gil/catkin_ws/src/gd_msgs

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/gil/catkin_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gd_msgs/msg" TYPE FILE FILES
    "/home/gil/catkin_ws/src/gd_msgs/msg/State.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/FullState.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVel.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateVelT.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/FullStateSim.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/RCValue.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/RCArray.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegs.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/BatteryRegsArray.msg"
    "/home/gil/catkin_ws/src/gd_msgs/msg/MotorSystemVariables.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gd_msgs/srv" TYPE FILE FILES
    "/home/gil/catkin_ws/src/gd_msgs/srv/SetInput.srv"
    "/home/gil/catkin_ws/src/gd_msgs/srv/SetMode.srv"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gd_msgs/cmake" TYPE FILE FILES "/home/gil/catkin_ws/build/gd_msgs/catkin_generated/installspace/gd_msgs-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/gil/catkin_ws/devel/include/gd_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/gil/catkin_ws/devel/share/roseus/ros/gd_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/gil/catkin_ws/devel/share/common-lisp/ros/gd_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/gil/catkin_ws/devel/share/gennodejs/ros/gd_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python" -m compileall "/home/gil/catkin_ws/devel/lib/python2.7/dist-packages/gd_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/gil/catkin_ws/devel/lib/python2.7/dist-packages/gd_msgs")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/gil/catkin_ws/build/gd_msgs/catkin_generated/installspace/gd_msgs.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gd_msgs/cmake" TYPE FILE FILES "/home/gil/catkin_ws/build/gd_msgs/catkin_generated/installspace/gd_msgs-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gd_msgs/cmake" TYPE FILE FILES
    "/home/gil/catkin_ws/build/gd_msgs/catkin_generated/installspace/gd_msgsConfig.cmake"
    "/home/gil/catkin_ws/build/gd_msgs/catkin_generated/installspace/gd_msgsConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gd_msgs" TYPE FILE FILES "/home/gil/catkin_ws/src/gd_msgs/package.xml")
endif()

