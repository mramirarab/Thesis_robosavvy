# CMake generated Testfile for 
# Source directory: /home/gil/catkin_ws/src
# Build directory: /home/gil/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(gd_msgs)
subdirs(velodyne/velodyne)
subdirs(velodyne/velodyne_msgs)
subdirs(facial_recognition)
subdirs(lidartracking)
subdirs(movement)
subdirs(normalgenerator)
subdirs(segmentation)
subdirs(select_person)
subdirs(testing)
subdirs(velodyne/velodyne_driver)
subdirs(velodyne/velodyne_laserscan)
subdirs(velodyne/velodyne_pointcloud)
subdirs(xyztracker)
