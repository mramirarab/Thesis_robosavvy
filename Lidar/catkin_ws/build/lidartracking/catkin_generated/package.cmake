set(_CATKIN_CURRENT_PACKAGE "lidartracking")
set(lidartracking_VERSION "0.0.0")
set(lidartracking_MAINTAINER "gil <gil@todo.todo>")
set(lidartracking_PACKAGE_FORMAT "2")
set(lidartracking_BUILD_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(lidartracking_BUILD_EXPORT_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(lidartracking_BUILDTOOL_DEPENDS "catkin")
set(lidartracking_BUILDTOOL_EXPORT_DEPENDS )
set(lidartracking_EXEC_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(lidartracking_RUN_DEPENDS "roscpp" "rospy" "sensor_msgs" "std_msgs")
set(lidartracking_TEST_DEPENDS )
set(lidartracking_DOC_DEPENDS )
set(lidartracking_URL_WEBSITE "")
set(lidartracking_URL_BUGTRACKER "")
set(lidartracking_URL_REPOSITORY "")
set(lidartracking_DEPRECATED "")