
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/openni_grabber.h>
#include <pcl/console/parse.h>
#include <pcl/common/time.h>
#include <pcl/common/centroid.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>

#include <pcl/search/pcl_search.h>
#include <pcl/common/transforms.h>

#include <boost/format.hpp>

#include <pcl/tracking/tracking.h>
#include <pcl/tracking/particle_filter.h>
#include <pcl/tracking/kld_adaptive_particle_filter_omp.h>
#include <pcl/tracking/particle_filter_omp.h>
#include <pcl/tracking/coherence.h>
#include <pcl/tracking/distance_coherence.h>
#include <pcl/tracking/normal_coherence.h>
#include <pcl/tracking/hsv_color_coherence.h>
#include <pcl/tracking/approx_nearest_pair_point_cloud_coherence.h>
#include <pcl/tracking/nearest_pair_point_cloud_coherence.h>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

using namespace pcl::tracking;

typedef pcl::PointXYZRGBA RefPointType;
typedef ParticleXYZRPY ParticleT;
typedef pcl::PointCloud<pcl::PointXYZRGBA> Cloud;
typedef Cloud::Ptr CloudPtr;
typedef Cloud::ConstPtr CloudConstPtr;
typedef ParticleFilterTracker<RefPointType, ParticleT> ParticleFilter;

CloudPtr cloud_pass_;
CloudPtr cloud_pass_downsampled_;
//CloudPtr target_cloud;

boost::mutex mtx_;
boost::shared_ptr<ParticleFilter> tracker_;
//bool new_cloud_;
//double downsampling_grid_size_;
int counter;
int downsampling_grid_size_ =  0.01;
int model_set;

ros::Publisher pub;
ros::Publisher pub2;
//Filter along a specified dimension
void filterPassThrough (const CloudConstPtr &cloud, Cloud &result)
{
  pcl::PassThrough<pcl::PointXYZRGBA> pass;
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0.0, 10.0);
  pass.setKeepOrganized (false);
  pass.setInputCloud (cloud);
  pass.filter (result);
}


void gridSampleApprox (const CloudConstPtr &cloud, Cloud &result, double leaf_size)
{
  pcl::ApproximateVoxelGrid<pcl::PointXYZRGBA> grid;
  grid.setLeafSize (static_cast<float> (leaf_size), static_cast<float> (leaf_size), static_cast<float> (leaf_size));
  grid.setInputCloud (cloud);
  grid.filter (result);
}




//OpenNI Grabber's cloud Callback function
void
track_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{
	if(model_set == 1){
		pcl::PCLPointCloud2 inputcloud;
		CloudPtr cloud(new Cloud);

	// Convert to PCL data type
		pcl_conversions::toPCL(*input, inputcloud);
		pcl::fromPCLPointCloud2(inputcloud, *cloud);
		boost::mutex::scoped_lock lock (mtx_);
		cloud_pass_.reset (new Cloud);
		cloud_pass_downsampled_.reset (new Cloud);
		filterPassThrough (cloud, *cloud_pass_);
		gridSampleApprox (cloud_pass_, *cloud_pass_downsampled_, downsampling_grid_size_);

  	//Track the object
		tracker_->setInputCloud (cloud_pass_downsampled_);
		tracker_->compute ();

    //add the parameters we want
		ParticleT positionstate = tracker_->getResult();
		Eigen::Affine3f movement = tracker_->toEigenMatrix(positionstate);

	//update()  //update the model
		geometry_msgs::Vector3 movement_message;
		movement_message.x = movement.translation()[0];
		movement_message.y = movement.translation()[1];
		movement_message.z = movement.translation()[2];
		pub.publish(movement_message);
		//tracker_->update();



	//	pcl::PointCloud<pcl::Pointmessagecloud> = tracker_->getParticles();
	//	Eigen::Vector4f possiblesame;
	//	pcl::compute3DCentroid<pcl::PointXYZRPY> (*messagecloud, possiblesame);

		//Eigen::Affine3f possiblesame = tracker_->getTrans();
	//	movement_message.x = possiblesame[0];
	//	movement_message.y = possiblesame[1];
	//	movement_message.z = possiblesame[2];

    //	pub.publish(movement_message);
	//particlefiltertracker
		//std::cout << "difference in position = "<< movement.translation() << movement.linear() << "\n";
		//std::cout << "po1200ssibly the same position = "<< possiblesame.translation() << possiblesame.linear()<<"\n";
	//std::cout << "maybe this is possible ="<< tracker_->motion_<<"\n"; protected
	//calculate speed in relation to the robot
	}

}

void
model_cb (const sensor_msgs::PointCloud2ConstPtr& input)
{

	// Container for original & filtered data
	//pcl::PointCloud<pcl::PointXYZ> cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PCLPointCloud2 inputcloud;
	CloudPtr cloud(new Cloud);
	//std::cout << "do \n" <<std::flush;
	// Convert to PCL data type

	pcl_conversions::toPCL(*input, inputcloud);
	//std::cout << "i \n" <<std::flush;
	pcl::fromPCLPointCloud2(inputcloud, *cloud);    //RGB missing might be a problem
	//std::cout << "i \n" <<std::flush;
    //prepare the model of tracker's target
    Eigen::Vector4f c;
    Eigen::Affine3f trans = Eigen::Affine3f::Identity ();
    CloudPtr transed_ref (new Cloud);
    CloudPtr transed_ref_downsampled (new Cloud);
    //std::cout << "crash \n" <<std::flush;

    pcl::compute3DCentroid<RefPointType> (*cloud, c);
    trans.translation ().matrix () = Eigen::Vector3f (c[0], c[1], c[2]);
    pcl::transformPointCloud<RefPointType> (*cloud, *transed_ref, trans.inverse());
    gridSampleApprox (transed_ref, *transed_ref_downsampled, downsampling_grid_size_);
    //std::cout << "here \n" <<std::flush;
    //set reference model and trans
    tracker_->setReferenceCloud (transed_ref_downsampled);
    tracker_->setTrans (trans);
    model_set = 1;
    //std::cout << "model received \n";
}




int
main (int argc, char** argv)
{

	counter = 0;

	  //Set parameters
	  std::vector<double> default_step_covariance = std::vector<double> (6, 0.015 * 0.015);
	  default_step_covariance[3] *= 40.0;
	  default_step_covariance[4] *= 40.0;
	  default_step_covariance[5] *= 40.0;

	  std::vector<double> initial_noise_covariance = std::vector<double> (6, 0.00001);
	  std::vector<double> default_initial_mean = std::vector<double> (6, 0.0);

	  boost::shared_ptr<KLDAdaptiveParticleFilterOMPTracker<RefPointType, ParticleT> > tracker
	    (new KLDAdaptiveParticleFilterOMPTracker<RefPointType, ParticleT> (8));

	  ParticleT bin_size;
	  bin_size.x = 0.1f;
	  bin_size.y = 0.1f;
	  bin_size.z = 0.1f;
	  bin_size.roll = 0.1f;
	  bin_size.pitch = 0.1f;
	  bin_size.yaw = 0.1f;


	  //Set all parameters for  KLDAdaptiveParticleFilterOMPTracker
	  tracker->setMaximumParticleNum (1000);
	  tracker->setDelta (0.99);
	  tracker->setEpsilon (0.2);
	  tracker->setBinSize (bin_size);

	  //Set all parameters for  ParticleFilter
	  tracker_ = tracker;
	  tracker_->setTrans (Eigen::Affine3f::Identity ());
	  tracker_->setStepNoiseCovariance (default_step_covariance);
	  tracker_->setInitialNoiseCovariance (initial_noise_covariance);
	  tracker_->setInitialNoiseMean (default_initial_mean);
	  tracker_->setIterationNum (1);
	  tracker_->setParticleNum (600);
	  tracker_->setResampleLikelihoodThr(0.00);
	  tracker_->setUseNormal (false);


	  //Setup coherence object for tracking
	  ApproxNearestPairPointCloudCoherence<RefPointType>::Ptr coherence = ApproxNearestPairPointCloudCoherence<RefPointType>::Ptr
	    (new ApproxNearestPairPointCloudCoherence<RefPointType> ());

	  boost::shared_ptr<DistanceCoherence<RefPointType> > distance_coherence
	    = boost::shared_ptr<DistanceCoherence<RefPointType> > (new DistanceCoherence<RefPointType> ());
	  coherence->addPointCoherence (distance_coherence);


	/*  //might improve the system
	  boost::shared_ptr<NormalCoherence<RefPointType> > normal_coherence
	    = boost::shared_ptr<NormalCoherence<RefPointType> > (new NormalCoherence<RefPointType> ());
	  coherence->addPointCoherence (normal_coherence);
*/

	  boost::shared_ptr<pcl::search::Octree<RefPointType> > search (new pcl::search::Octree<RefPointType> (0.01));
	  coherence->setSearchMethod (search);
	  coherence->setMaximumDistance (0.01);


	  tracker_->setCloudCoherence (coherence);


  // Initialize ROS
  ros::init (argc, argv, "tracker");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub  = nh.subscribe ("/velodyne_points", 1, track_cb );					// track the person based on the prepared model
  ros::Subscriber submodel = nh.subscribe ("/right_person", 1, model_cb);				// prepare the model

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<geometry_msgs::Vector3> ("/first_option", 1);
  //  pub2 = nh.advertise<geometry_msgs::Vector3> ("/second_option", 1);


  // Spin
  ros::spin ();
}













