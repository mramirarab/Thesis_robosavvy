
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Twist.h>
#include "std_msgs/Bool.h"
#include <ros/ros.h>
#include <math.h>
using namespace std;
ros::Publisher move;
geometry_msgs::Twist output_speed;
float last_error = 0;
float last_angle = 0;
float integ = 0;
float angle_integ = 0;
float error = 0;
float kp = 0;
float kd = 0;
float ki = 0;
float akp = 0;
float akd = 0;
float aki = 0;



void firstCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{
	geometry_msgs::Vector3 accel;
	//PID controller
	float distance;
	cout<<"x =" <<msg->x<< "y = "<<msg->y<<"z = " <<msg->z<<"\n";
	distance  = sqrt(pow(msg->x,2) + pow(msg->y,2));
	error = distance -1.7;



		integ = integ + error;
		output_speed.linear.x = kp*error + kd*(error-last_error) + ki*integ;
		if(output_speed.linear.x > 1) output_speed.linear.x = 1;
		if(output_speed.linear.x < -1) output_speed.linear.x = -1;

	float angle = atan(msg->y/msg->x);

	angle_integ = angle_integ + angle;
	output_speed.angular.z = akp*angle + akd*(angle-last_angle) + aki*angle_integ;
	if(output_speed.angular.z > 0.6) output_speed.angular.z = 0.6;
	if(output_speed.angular.z < -0.6) output_speed.angular.z = -0.6;

	move.publish(output_speed);
	last_angle = angle;
	last_error = error;
}


void PIDCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{

	kp = msg->x;
	ki = msg->y;
	kd = msg->z;
	cout << "received new values \n"<<flush;



}

void angleCallback(const geometry_msgs::Vector3::ConstPtr& msg)
{

	akp = msg->x;
	aki = msg->y;
	akd = msg->z;
	cout << "received new angular values \n"<<flush;



}



int main(int argc, char **argv){
	ros::init(argc, argv, "movement");
	ros::NodeHandle n;
	ros::Subscriber sub = n.subscribe("/movement", 1, firstCallback);
	ros::Subscriber sub2 = n.subscribe("/PID_values", 1,PIDCallback);
	ros::Subscriber sub3 = n.subscribe("/PID_angle", 1,angleCallback);

	move = n.advertise<geometry_msgs::Twist> ("/cmd_vel", 1);
	ros::spin ();
}
