#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/approximate_voxel_grid.h>

#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/time.h>
#include <memory>

ros::Publisher pub;
ros::Publisher man;

void
cloud_cb (const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
// std::cout << "received";
  // Container for original & filtered data
  pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2;
  pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
  //pcl::PCLPointCloud2::Ptr cloud_filtered (new pcl::PCLPointCloud2 ());
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_xyz (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>);

//std::shqred_ptr<> = std::make_shared<>(à)
  // Convert to PCL data type
  pcl_conversions::toPCL(*cloud_msg, *cloud);

 /* // Perform the downsampling
  pcl::VoxelGrid<pcl::PCLPointCloud2> sor;
  sor.setInputCloud (cloudPtr);
  sor.setLeafSize (0.005, 0.005, 0.005);
  sor.filter (*cloud_filtered);
  pcl::fromPCLPointCloud2(*cloud_filtered,*filtered_xyz);
  */
  
  pcl::fromPCLPointCloud2(*cloud,*cloud_xyz);
  pcl::ApproximateVoxelGrid<pcl::PointXYZ> grid;
  grid.setLeafSize (0.005, 0.005, 0.005);
  grid.setInputCloud (cloud_xyz);
  grid.filter (*filtered_xyz);
  


  // Creating the KdTree object for the search method of the extraction
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (filtered_xyz);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance (0.1);
  ec.setMinClusterSize (200);      //aanpassen in tekst
  ec.setMaxClusterSize (20000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (filtered_xyz);
  ec.extract (cluster_indices);

  //int j = 0;
   for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
   {
     pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZ>);
     for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
       cloud_cluster->points.push_back (filtered_xyz->points[*pit]); 
     cloud_cluster->width = cloud_cluster->points.size ();
     cloud_cluster->height = 1;
     cloud_cluster->is_dense = true;


     float min_x = cloud_cluster->points[0].x, min_y = cloud_cluster->points[0].y, min_z = cloud_cluster->points[0].z, max_x = cloud_cluster->points[0].x, max_y = cloud_cluster->points[0].y, max_z = cloud_cluster->points[0].z;
      	for (size_t i = 1; i < cloud_cluster->points.size (); ++i){
            if(cloud_cluster->points[i].x <= min_x )
                min_x = cloud_cluster->points[i].x;
            else if(cloud_cluster->points[i].y <= min_y )
                min_y = cloud_cluster->points[i].y;
            else if(cloud_cluster->points[i].z <= min_z )
                min_z = cloud_cluster->points[i].z;
            else if(cloud_cluster->points[i].x >= max_x )
                max_x = cloud_cluster->points[i].x;
            else if(cloud_cluster->points[i].y >= max_y )
                max_y = cloud_cluster->points[i].y;
            else if(cloud_cluster->points[i].z >= max_z )
                max_z = cloud_cluster->points[i].z;
        }

        if(max_y-min_y < 1.2 && max_x-min_x < 1.2 ){
        	//std::cout<<"possible_human";
        	  sensor_msgs::PointCloud2 human;
        	  pcl::PCLPointCloud2* pclhuman = new pcl::PCLPointCloud2;


        	  pcl::toPCLPointCloud2(*cloud_cluster, *pclhuman);
        	  pcl_conversions::fromPCL(*pclhuman, human);

        	  // Publish the data
        	  man.publish(human);

        }

  // Convert to ROS data type
  sensor_msgs::PointCloud2 output;
  pcl::PCLPointCloud2* outputcloud = new pcl::PCLPointCloud2;


  pcl::toPCLPointCloud2(*cloud_cluster, *outputcloud);
  pcl_conversions::fromPCL(*outputcloud, output);

  // Publish the data
  pub.publish(output);

}
}
int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "segmentation");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/velodyne_points", 1, cloud_cb);

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud2> ("/clustered_input", 1);
  man = nh.advertise<sensor_msgs::PointCloud2> ("/possible_humans", 1);


  // Spin
  ros::spin ();
}

