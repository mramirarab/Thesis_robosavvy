#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <boost/format.hpp>


ros::Publisher pub;
ros::Publisher man;

void cloud_cb(const sensor_msgs::PointCloud2ConstPtr& input){
	pcl::PCLPointCloud2 inputcloud;

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
// Convert to PCL data type
	pcl_conversions::toPCL(*input, inputcloud);
	pcl::fromPCLPointCloud2(inputcloud, *cloud);



  // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::PointNormal> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  pcl::PointCloud<pcl::PointNormal>::Ptr cloud_normals (new pcl::PointCloud<pcl::PointNormal>);

  // Use all neighbors in a sphere of radius 3cm
  ne.setRadiusSearch (0.03);

  // Compute the features
  ne.compute (*cloud_normals);
	sensor_msgs::PointCloud2 output;
	pcl::PCLPointCloud2* outputcloud = new pcl::PCLPointCloud2;

	pcl::toPCLPointCloud2(*cloud_normals, *outputcloud);
	pcl_conversions::fromPCL(*outputcloud, output);
	pub.publish(output);


  // cloud_normals->points.size () should have the same size as the input cloud->points.size ()*
}

void model_cb(const sensor_msgs::PointCloud2ConstPtr& input){
	pcl::PCLPointCloud2 inputcloud;

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
// Convert to PCL data type
	pcl_conversions::toPCL(*input, inputcloud);
	pcl::fromPCLPointCloud2(inputcloud, *cloud);



  // Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::PointNormal> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  pcl::PointCloud<pcl::PointNormal>::Ptr cloud_normals (new pcl::PointCloud<pcl::PointNormal>);

  // Use all neighbors in a sphere of radius 3cm
  ne.setRadiusSearch (0.03);


  // Compute the features

  ne.compute (*cloud_normals);
	sensor_msgs::PointCloud2 output;
	pcl::PCLPointCloud2* outputcloud = new pcl::PCLPointCloud2;

	pcl::toPCLPointCloud2(*cloud_normals, *outputcloud);
	pcl_conversions::fromPCL(*outputcloud, output);
	man.publish(output);


  // cloud_normals->points.size () should have the same size as the input cloud->points.size ()*
}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "normal_generator");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub = nh.subscribe ("/velodyne_points", 1, cloud_cb);
  ros::Subscriber sub2 = nh.subscribe ("/right_person", 1, model_cb);

  // Create a ROS publisher for the output point cloud
  pub = nh.advertise<sensor_msgs::PointCloud2> ("/velodyne_points_normal", 1);
  man = nh.advertise<sensor_msgs::PointCloud2> ("/right_person_normal", 1);


  // Spin
  ros::spin ();
}
