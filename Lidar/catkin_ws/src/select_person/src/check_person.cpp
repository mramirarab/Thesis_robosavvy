#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include <pcl/common/centroid.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>

bool right_person = false;
ros::Publisher man;

void faceCallback(const std_msgs::Bool::ConstPtr& msg)
{
//	  std::cout << "message received \n" <<std::flush;
	  right_person = msg->data;

}

void personcallback (const sensor_msgs::PointCloud2ConstPtr& input)
{
	//std::cout << "responding" << std::flush;
	if(right_person ==true){
		pcl::PCLPointCloud2 inputcloud;
		pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

		// Convert to PCL data type

		pcl_conversions::toPCL(*input, inputcloud);
		pcl::fromPCLPointCloud2(inputcloud, *cloud);

		Eigen::Vector4f c;
		pcl::compute3DCentroid<pcl::PointXYZ> (*cloud, c);
		if(c[0] < 2 && c[0]>0 &&c[1]>-0.5 &&c[1]<0.5){
			sensor_msgs::PointCloud2 output;
			pcl::PCLPointCloud2* outputcloud = new pcl::PCLPointCloud2;

			pcl::toPCLPointCloud2(*cloud, *outputcloud);
			pcl_conversions::fromPCL(*outputcloud, output);
			man.publish(output);
			std::cout << "model centroid position = "<< c[0] << c[1] << c[2] << "\n";
			right_person = false;
		}

	}

}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "check_person");

  ros::NodeHandle n;

  ros::Subscriber subamir = n.subscribe("/face", 1, faceCallback);
  ros::Subscriber sub = n.subscribe("/possible_humans", 1, personcallback);

  man = n.advertise<sensor_msgs::PointCloud2> ("/right_person", 1);
  //std::cout << "here" << std::flush;
  ros::spin ();


}
