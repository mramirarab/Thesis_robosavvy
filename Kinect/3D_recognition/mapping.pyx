def mapPixels(depth):
    cdef int yD, xD, theX, theY, xRGB, yRGB
    cdef float zD, yzD, xzD, currentDepth
    cdef double Xr, Yr, Zr
    cdef float cxRGB = 322.578969
    cdef float cyRGB = 211.079824
    cdef float fxRGB = 654.746610
    cdef float fyRGB = 685.387083
    cdef int minDistance = 400
    cdef int maxDistance = 2000
    pointMap = {}
    for yD in range(480):
        for xD in range(640):
            zD =  depth[yD*640+xD]
            if zD < minDistance or zD > maxDistance:
                neighbours = []
                for theX in range(-2,3):
                    for theY in range(-2 ,3):
                        try:
                            currentDepth = depth[(yD+theY)*640+xD+theX]
                            if maxDistance > currentDepth > minDistance:
                                neighbours.append(currentDepth)
                        except:
                            pass
                neighbours = sorted(neighbours)
                # if len(neighbours)%2:
                try:
                    zD = neighbours[len(neighbours) /2]
                except:
                    continue
                # else:
                    # if not neighbours:
                        # continue
                    # else:
                        # zD = (neighbours[len(neighbours) / 2] - neighbours[len(neighbours) / 2 - 1]) / 2
                        # zD = neighbours[len(neighbours) / 2]
            yzD = yD * zD
            xzD = xD * zD
            Xr = 0.1387850108e-2 * xzD + 0.3665893987e-5 * yzD - 0.4744667481 * zD + 0.2495798001e-1
            Yr = -0.3758696511e-5 * xzD + 0.1327577647e-2 * yzD - 0.2512203978 * zD - 0.5320597004e-3
            Zr = 0.1704121627e-4 * xzD - 0.5736086706e-5 * yzD + 0.9953583655 * zD - 0.5969659003e-2
            xRGB = <int>(Xr * fxRGB / Zr + cxRGB)
            yRGB = <int>(Yr * fyRGB / Zr + cyRGB)
            pointMap.update({(xRGB, yRGB): [xD, yD, zD, <int>Xr, <int>Yr, <int>Zr]})
    return pointMap

def getRGBCoordinate(int x,int y,depth):
    cdef int yD, xD, theX, theY, xRGB, yRGB
    cdef float zD, yzD, xzD, currentDepth
    cdef double Xr, Yr, Zr
    cdef float cxRGB = 322.578969
    cdef float cyRGB = 211.079824
    cdef float fxRGB = 654.746610
    cdef float fyRGB = 685.387083
    cdef int minDistance = 400
    cdef int maxDistance = 2000
    cdef int vicinity = 50
    for yD in range(y -vicinity , y + vicinity+1):
        for xD in range(x - vicinity, x+vicinity+1):
            try:
                zD =  depth[yD*640+xD]
                if zD < minDistance or zD > maxDistance:
                    neighbours = []
                    for theX in range(-1,2):
                        for theY in range(-1 ,2):
                            try:
                                currentDepth = depth[(yD+theY)*640+xD+theX]
                                if maxDistance > currentDepth > minDistance:
                                    neighbours.append(currentDepth)
                            except:
                                pass
                    neighbours = sorted(neighbours)
                    zD = neighbours[len(neighbours) /2]
            except:
                continue
            yzD = yD * zD
            xzD = xD * zD
            Xr = 0.1387850108e-2 * xzD + 0.3665893987e-5 * yzD - 0.4744667481 * zD + 0.2495798001e-1
            Yr = -0.3758696511e-5 * xzD + 0.1327577647e-2 * yzD - 0.2512203978 * zD - 0.5320597004e-3
            Zr = 0.1704121627e-4 * xzD - 0.5736086706e-5 * yzD + 0.9953583655 * zD - 0.5969659003e-2
            xRGB = <int>(Xr * fxRGB / Zr + cxRGB)
            yRGB = <int>(Yr * fyRGB / Zr + cyRGB)
            if xRGB == x and yRGB == y:
                return [xD, yD, zD, <int>Xr, <int>Yr, <int>Zr]