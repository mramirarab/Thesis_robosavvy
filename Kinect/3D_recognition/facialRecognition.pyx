import math


def calculateHitRate(newFace , referenceFace):
    cdef int hit = 0
    cdef int totalCheck = 0
    cdef double lengthDiff
    cdef float currentSlop,savedSlop,angleDiff,hitRate
    for pointPosition1, currentPoint1 in newFace.items():#for each element in the collection
        try:
            savedPoint1 = referenceFace[1][pointPosition1]
            for pointPosition2, currentPoint2 in newFace.items():#compare that element with all other points
                if pointPosition1 != pointPosition2:#except for the point itself
                    savedPoint2 = referenceFace[1][pointPosition2]
                    totalCheck += 1
                    first = ((savedPoint1[0] - savedPoint2[0]) ** 2 + (savedPoint1[1] - savedPoint2[1]) ** 2 + (savedPoint1[2] - savedPoint2[2]) ** 2) ** (1 / 2)
                    second = ((currentPoint1[0] - currentPoint2[0]) ** 2 + (currentPoint1[1] - currentPoint2[1]) ** 2 + (currentPoint1[2] - currentPoint2[2]) ** 2) ** (1 / 2)
                    print([first , second])
                    lengthDiff = abs(first - second)
                    if currentPoint1[0] - currentPoint2[0] != 0:
                        currentSlop = math.degrees(math.atan((currentPoint1[1] - currentPoint2[1]) / (currentPoint1[0] - currentPoint2[0])))
                    else:
                        currentSlop = 90
                    if savedPoint1[0] - savedPoint2[0] != 0:
                        savedSlop = math.degrees(math.atan((savedPoint1[1] - savedPoint2[1]) / (savedPoint1[0] - savedPoint2[0])))
                    else:
                        savedSlop = 90
                    angleDiff = abs(savedSlop - currentSlop)
                    if not 32 <= pointPosition1 <= 36 and not 32 <= pointPosition2 <= 36 and angleDiff <= 90:
                        if lengthDiff < 5 and angleDiff < 20:
                            hit += 1
                        else:
                            print "@ ", pointPosition1, ' and ', pointPosition2, ' length diff is %.2f and slop diff is %.0f' % (lengthDiff, angleDiff)
                    else:
                        if lengthDiff < 10:
                            hit += 1
                        else:
                            print "@ ", pointPosition1, ' and ', pointPosition2, ' length diff is %.2f' % (lengthDiff)
        except Exception as e:
            pass
    if totalCheck:
        hitRate = hit*100.0/totalCheck
    else:
        hitRate = 0
    return hitRate