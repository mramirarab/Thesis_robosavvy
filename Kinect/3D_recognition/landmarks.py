import math
import cv2
import imutils
import numpy as np
import dlib
import freenect
import frame_convert2
import plyvel
import os
from dataStorage import encodeKey,decodeValue,decodeKey,encodeValue
import kinectConfig
import time
import pyximport; pyximport.install()
from mapping import mapPixels,getRGBCoordinate
from facialRecognition import calculateHitRate

def land2coords(landmarks, dtype="int"):
    # initialize the list of tuples
    # (x, y)-coordinates
    coords = np.zeros((68, 2), dtype=dtype)

    # loop over the 68 facial landmarks and convert them
    # to a 2-tuple of (a, b)-coordinates
    for i in range(0, 68):
        coords[i] = (landmarks.part(i).x, landmarks.part(i).y)

    # return the list of (a, b)-coordinates
    return coords


# main Function
if __name__ == "__main__":
    kinectAngle = 20
    distanceThreshold = 100
    saved = []
    train = False
    showNumbers = False
    showFrame = False
    showDetails = True

    db = plyvel.DB(os.getcwd(),create_if_missing=True)
    # loading dlib's Hog Based face detector
    face_detector = dlib.get_frontal_face_detector()

    ctx = freenect.init()
    dev = freenect.open_device(ctx, freenect.num_devices(ctx) - 1)

    if not dev:
        freenect.error_open_device()
    freenect.set_tilt_degs(dev, kinectAngle)
    freenect.shutdown(ctx)

    # loading dlib's 68 points-shape-predictor
    # get file:shape_predictor_68_face_landmarks.dat from
    # link: http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2
    print('reading data file ...')
    landmark_predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')
    print('data file is read')


    while True:
        print('new frame ...')
        t0 = time.perf_counter()
        valid = True
        hitRate = 0
        if showDetails:
            print('reading video frame...')
        frame, _ = freenect.sync_get_video(0,freenect.VIDEO_RGB)
        if showDetails:
            print('reading depth frame...')
            showDetails = False
        rawDepth, timeStamp = freenect.sync_get_depth(0, freenect.DEPTH_MM)
        frame = imutils.resize(frame, width=640)
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        pointMap = {}
        depthList = []
        for y in rawDepth:
            depthList += y.tolist()
        depth = frame_convert2.pretty_depth_cv(rawDepth)
        del rawDepth
        t1 = time.perf_counter()
        # pointMap = mapPixels(depthList)
        t2 = time.perf_counter()
        # detecting faces
        face_boundaries = face_detector(frame_gray, 0)
        points = {}
        for (enum, face) in enumerate(face_boundaries):
            # draw a rectangle on the face portion of image
            x = face.left()
            y = face.top()
            w = face.right() - x
            h = face.bottom() - y
            isImage = False

            # Now when we have our ROI(face area) let's
            # predict and draw landmarks
            landmarks = landmark_predictor(frame_gray, face)
            # converting co-ordinates to NumPy array
            landmarks = land2coords(landmarks)
            counter = 0
            zAngle = 0
            yAngle = 0
            pos20 = 0.0
            var14 = 0.0
            var49 = 0.0
            var4 = 0.0
            for (a, b) in landmarks:
                # Drawing points on face
                thePoint = getRGBCoordinate(a , b, depthList)
                # thePoint = pointMap.get((a,b))
                counter += 1
                cv2.circle(frame, (a, b), 2, (255, 0, 0), -1)  # rgb
                if counter == 4:
                    var4 = a
                if counter == 14:
                    var14 = a
                if counter == 49:
                    var49 = a
                if counter == 55:
                    yAngle = (a-var14)-(var4-var49)
                    del var4
                    del var14
                    del var49
                if thePoint:
                    if showNumbers:
                        cv2.circle(frame, (thePoint[0], thePoint[1]), 2, (255, 255, 255), -1)  # rgb
                    if counter == 20 or counter == 25:
                        if counter == 20:
                            po20 = (thePoint[3], thePoint[4])
                        else:
                            try:
                                zAngle = round(math.degrees(math.atan((thePoint[4] - po20[1]) / (thePoint[3] - po20[0]))), 0)  #arctan((Yr25-Yr20)/(Xr25-Xr20))
                                del po20
                            except:
                                print('image?')
                    if counter < 61 and not counter <= 7 and not 17 >= counter >= 11:
                        points.update({counter: [thePoint[3], thePoint[4], thePoint[5],thePoint[0],thePoint[1],thePoint[2]]})#Xr,Yr,Zr,xD,yD,zD
                        cv2.circle(depth, (thePoint[0], thePoint[1]), 2, (0), -1)
                        if(showNumbers):
                            cv2.putText(depth, str(counter), (thePoint[0], thePoint[1]),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0))
                            cv2.putText(frame, str(counter), (a, b),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0))#rgb
            try:
                currentNose = points.get(31)
                chin = points.get(9)
                if currentNose == None or currentNose == None:
                    print('not useful frame')
                    print(points)
                    valid = False
                    continue
                xAngle = round(currentNose[2] - points.get(9)[2], 0)
            except Exception as e:
                print(e)
                continue
            valid = xAngle <= 10 and -35 < yAngle < 35
            if not valid:
                print('xAngle is ', xAngle <= -1 ,' and yAngle is ', -45 < yAngle < 40 )
            foundIt = []
            for s in saved:
                if (abs(xAngle - s[0][0]) <= 2 and abs(yAngle - s[0][1]) <= 2 and abs(zAngle - s[0][2]) <= 2) and abs(currentNose[2] - s[0][3][2]) <= distanceThreshold:
                    foundIt = s
                    break
                # else:
                    # print('why points not found?')
                    # if not abs(xAngle - s[0][0]) <= 10:
                    #     print('xAngle not within range')
                    # if not abs(yAngle - s[0][1]) <= 10:
                    #     print('yAngle not within range')
                    # if not abs(zAngle - s[0][2]) <= 10:
                    #     print('zAngle not within range')
            if train and not foundIt and valid:
                saved += [[[xAngle, yAngle , zAngle, currentNose], points]]
                print("array size is ", len(saved))
            if foundIt and not train and valid:
                # hitRate = calculateHitRate(points , foundIt)
                hit = 0
                totalCheck = 0
                for pointPosition1, currentPoint1 in points.items():  # for each element in the collection
                    try:
                        savedPoint1 = foundIt[1][pointPosition1]
                    except:
                        continue
                    for pointPosition2, currentPoint2 in points.items():  # compare that element with all other points
                        if pointPosition1 != pointPosition2 and pointPosition1 != 10 and pointPosition2 != 10 and pointPosition1 != 8 and pointPosition2 !=8:  # except for the point itself
                            try:
                                savedPoint2 = foundIt[1][pointPosition2]
                            except:
                                continue
                            first = ((savedPoint1[0] - savedPoint2[0]) ** 2 + (
                                        savedPoint1[1] - savedPoint2[1]) ** 2 + (
                                                 savedPoint1[2] - savedPoint2[2]) ** 2) ** (1 / 2)
                            second = ((currentPoint1[0] - currentPoint2[0]) ** 2 + (
                                        currentPoint1[1] - currentPoint2[1]) ** 2 + (
                                                  currentPoint1[2] - currentPoint2[2]) ** 2) ** (1 / 2)
                            lengthDiff = abs(first - second)
                            if currentPoint1[0] - currentPoint2[0] != 0:
                                currentSlop = math.degrees(math.atan(
                                    (currentPoint1[1] - currentPoint2[1]) / (currentPoint1[0] - currentPoint2[0])))
                            else:
                                currentSlop = 90
                            if savedPoint1[0] - savedPoint2[0] != 0:
                                savedSlop = math.degrees(math.atan(
                                    (savedPoint1[1] - savedPoint2[1]) / (savedPoint1[0] - savedPoint2[0])))
                            else:
                                savedSlop = 90
                            angleDiff = abs(savedSlop - currentSlop)
                            if not 32 <= pointPosition1 <= 36 and not 32 <= pointPosition2 <= 36 and angleDiff <= 90:
                                if lengthDiff <= 7 and angleDiff <= 20:
                                    hit += 1
                                else:
                                    print ("@ ", pointPosition1, ' and ', pointPosition2, ' length diff is %.2f and slop diff is %.0f' % (lengthDiff, angleDiff))
                            else:
                                if lengthDiff <= 7:
                                    hit += 1
                                else:
                                    print("@ ", pointPosition1, ' and ', pointPosition2, ' length diff is %.2f' % (lengthDiff))
                            if lengthDiff < 300:
                                totalCheck += 1
                if totalCheck:
                    print(totalCheck)
                    hitRate = hit * 100.0 / totalCheck
                else:
                    hitRate = 0
            elif not foundIt and not train:
                hitRate = -100.0
                # print([xAngle, yAngle , zAngle, nose[2]])

            if valid:
                thePoint = getRGBCoordinate(x, y , depthList)
                if thePoint == None:
                    thePoint = [x , y]
                # Writing face number on image
                # cv2.putText(frame, "Face :{}".format(enum + 1), (x - 10, y - 10),
                #             cv2.FONT_HERSHEY_SIMPLEX, 0.5, (200, 200, 128), 2)
                cv2.putText(frame, "nose is at %.0f" % (currentNose[2]), (thePoint[0], thePoint[1] - 50),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255 , 255 , 0), 0)
                cv2.putText(frame, "z rotation is %.2f" % (zAngle), (thePoint[0], thePoint[1] - 20),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255 , 255 , 0), 0)
                cv2.putText(frame, "y rotation is %.2f" % (yAngle), (thePoint[0] , thePoint[1] - 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255 , 255 , 0), 0)
                cv2.putText(frame, "x rotation is %.2f" % (xAngle), (thePoint[0] , thePoint[1] - 40),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255 , 255 , 0), 0)
                # if not [s for s in points if s[3] != 0]:
                #     cv2.putText(depth, "Face :{} is an image".format(enum + 1),
                #                 (x + 8, y - 20),
                #                 cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0), 2)
                # else:
                if True:
                    if saved:
                        if train:
                            cv2.putText(frame,"Face :{} is training".format(enum + 1) , (thePoint[0], thePoint[1] - 10),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0), 2)
                        else:
                            if hitRate > 90.0:
                                cv2.putText(frame, "Face :{} %.0f%s hit %s".format(enum + 1) %(hitRate , "%" ,"face recognized"), (thePoint[0], thePoint[1]-10),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,255,0), 2)
                            else:
                                cv2.putText(frame, "Face :{} %.0f%s hit".format(enum + 1) % (hitRate, "%"), (thePoint[0], thePoint[1]-10),
                                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 2)
                    else:
                        cv2.putText(frame, "Face :{}".format(enum + 1), (thePoint[0], thePoint[1]-10),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,255,0), 2)

                # Drawing Rectangle on face part
                cv2.rectangle(frame, (x, y), (x + w, y + h), (120, 160, 230), 2)
                # cv2.rectangle(depth, (thePoint[0] , thePoint[1] ), (thePoint[0] + w, thePoint[1] + h), (0,0,0), 2)

        tlast = time.perf_counter()
        # cv2.putText(depth, "%d fps and frame time %.2f" % (1 / (tlast - t0),(tlast - t0)), (5, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)
        cv2.putText(frame, "%d fps and frame time %.2f" % (1 / (tlast - t0), (tlast - t0)), (5, 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1)
        # print('full one frame time is ' , tlast-t0, 'the mapping took ' , t2-t1)
        # if showFrame:
        frame = frame_convert2.video_cv(frame)  # rgb
        cv2.imshow("frame", frame)  # rgb
        cv2.imshow("depth", depth)
        #  Stop if 'q' is pressed
        key = cv2.waitKey(1)
        if key == ord('q'):
            db.close()
            break
        #   if key t is pressed the training procedure will start
        elif key == ord('t'):
            train = not train
        elif key == ord('f'):
            saved = []
        elif key == ord('p'):
            print(train, " saved size -> ", len(saved))
        elif key == ord('n'):
            showNumbers = not showNumbers
        elif key == ord('s'):
            print('saving')
            #deleting all previous data
            it = db.iterator()
            for p in it:
                db.delete(p[0])
            #saving new points
            for p in saved:
                db.put(encodeKey(p[0]), encodeValue(p[1]))
        elif key == ord('l'):
            #loading
            saved = []
            it = db.iterator()
            for p in it:
                saved += [[decodeKey(p[0]), decodeValue(p[1])]]
                print('loading -> ', [p[0], p[1]])
        elif key == ord('o'):
            showFrame = not showFrame

