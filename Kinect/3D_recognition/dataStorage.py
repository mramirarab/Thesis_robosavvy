

def encodeKey(key):
    string = ''
    counter = 0
    for arg in key:
        if counter != 0:
            string += '-a-'
        if not type(arg) == list:
            string += str(arg)
        else:
            for p in arg:
                string = string + '-b-' + str(p)
        counter +=1
    return string.encode()


def decodeKey(bytes):
    bytes = bytes.decode()
    bytes = bytes.split('-a--b-')
    firstPart = list(map(float, bytes[0].split('-a-')))
    secondPart = list(map(float, bytes[1].split('-b-')))
    return firstPart+[secondPart]


def encodeValue(value):
    string = ''
    counter1 = 0
    for k,v in value.items():
        if counter1 != 0:
            string += '-a-'
        string = string + str(k) + '-a-'
        counter2 = 0
        for p in v:
            if counter2 != 0:
                string += '-b-'
            string += str(p)
            counter2 += 1
        counter1 += 1
    return string.encode()


def decodeValue(bytes):
    bytes = bytes.decode()
    bytes = bytes.split('-a-')
    final = {}
    for i in range(0 , len(bytes),2):
        final.update({int(bytes[i]) : list(map(float, bytes[i+1].split('-b-')))})
    return final




if __name__ == "__main__":
    key = [-25, 23, -1.0, [28, -12, 704, 371, 177, 704.0]]
    value = {8: [-5, 64, 757, 336, 254, 758.0], 9: [11, 64, 755, 352, 254, 756.0], 10: [26, 62, 767, 366, 252, 767.0]}
    print(key)
    key = encodeKey(key)
    print(key)
    key = decodeKey(key)
    print(key)
    print(value)
    value = encodeValue(value)
    print(value)
    value = decodeValue(value)
    print(value)
