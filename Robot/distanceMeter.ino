/*
*
* by Amir Arab
* upon the work of Dejan Nedelkovski,
* www.HowToMechatronics.com
*
*/
/*
* This program will set pin D13 high if any of the sensors sense distance less than 40cm otherwise low
*
*/

// defines pins numbers
const int trig_fr = 2;
const int echo_fr = 3;
const int trig_fl = 4;
const int echo_fl = 5;
const int trig_rr = 6;
const int echo_rr = 7;
const int trig_rl = 8;
const int echo_rl = 9;
const int indicator = 13;

void setup() {
  pinMode(trig_fr, OUTPUT); 
  pinMode(trig_fl, OUTPUT); 
  pinMode(trig_rr, OUTPUT); 
  pinMode(trig_rl, OUTPUT); 
  pinMode(echo_fr, INPUT); 
  pinMode(echo_fl, INPUT); 
  pinMode(echo_rr, INPUT); 
  pinMode(echo_rl, INPUT); 
  pinMode(indicator , OUTPUT);
//  Serial.begin(9600); // Starts the serial communication
}

void loop() {
//  unsigned long StartTime = millis();

  int distance_fr = 0;
  int distance_fl = 0;
  int distance_rr = 0;
  int distance_rl = 0;
  measure_distance(trig_fr , echo_fr , &distance_fr);
//  Serial.print("Distance fr: ");
//  Serial.print(distance_fr);
  measure_distance(trig_fl , echo_fl , &distance_fl);
//  Serial.print("\tDistance fl: ");
//  Serial.print(distance_fl);
  measure_distance(trig_rr , echo_rr , &distance_rr);
//  Serial.print("\tDistance rr: ");
//  Serial.print(distance_rr);
  measure_distance(trig_rl , echo_rl , &distance_rl);
//  Serial.print("\tDistance rl: ");
//  Serial.println(distance_rl);
  
//  unsigned long CurrentTime = millis();
//  Serial.println( CurrentTime - StartTime);
  if(distance_fr < 40 || distance_fl < 40 || distance_rr < 40 || distance_rl < 40){
    digitalWrite(indicator, HIGH);
  }else{
    digitalWrite(indicator, LOW);
  }
}

void measure_distance(const int trig, const int echo, int* distance){
  // Clears the trigPin
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);
  // Calculating the distance
  *distance = pulseIn(echo, HIGH)*0.034/2;

}
