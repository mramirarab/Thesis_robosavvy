import readchar
import rospy
from geometry_msgs.msg import Twist
import os

if __name__ == "__main__":
	rospy.init_node('robot_controller', anonymous=True)
	velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
	vel_msg = Twist()
	key = ''
	parked = False
	prev = ''
	xMovment = 0.2
	zRotation = 0.2
	print('to exit press e')
	while key!= 'e' and not parked:
		vel_msg.linear.x = 0
		vel_msg.linear.y = 0
		vel_msg.linear.z = 0
		vel_msg.angular.x = 0
		vel_msg.angular.y = 0
		vel_msg.angular.z = 0
		key = readchar.readkey()
		if key == '0':
			prev = ''
			vel_msg.linear.x = 0.0
			vel_msg.angular.z = 0.0
		elif key == readchar.key.UP:
			if prev == 'up':
				xMovment += 0.1
				print('xMovment = %.1f'%(xMovment))
			else:
				xMovment = 0.2
			prev = 'up'
			vel_msg.linear.x = xMovment
		elif key == readchar.key.DOWN:
			if prev == 'down':
				xMovment -= 0.1
				print('xMovment = %.1f'%(xMovment))
			else:
				xMovment = -0.2
			prev = 'down'
			vel_msg.linear.x = xMovment
		elif key == readchar.key.LEFT:
			if prev == 'left':
				zRotation += 0.1
				print('zRotation = %.1f'%(zRotation))
			else:
				zRotation = 0.2
			prev = 'left'
			vel_msg.angular.z = zRotation
		elif key == readchar.key.RIGHT:
			if prev == 'right':
				zRotation -= 0.1
				print('zRotation = %.1f'%(zRotation))
			else:
				zRotation = -0.2
			prev = 'right'
			vel_msg.angular.z = zRotation
		elif key == 'p':
			parked = True
			os.system('rostopic pub -1 /park std_msgs/Float32 "data: 1"')
		velocity_publisher.publish(vel_msg)
	
