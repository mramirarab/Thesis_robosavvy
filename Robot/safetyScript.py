import rospy
import time
from geometry_msgs.msg import Twist
import os
import sys
import select
import RPi.GPIO as io
   

def callback(data):
	global velocity_publisher
	safe = not io.input(7)
	if not safe:		
		vel_msg = Twist()
		vel_msg.linear.x = 0
		vel_msg.linear.y = 0
		vel_msg.linear.z = 0
		vel_msg.angular.x = 0
		vel_msg.angular.y = 0
		vel_msg.angular.z = 0
		velocity_publisher.publish(vel_msg)
		print('not safe')
	else:
		velocity_publisher.publish(data)

if __name__ == "__main__":
	stop = False
	io.setmode(io.BOARD)#use the numbering on the board
	io.setup(7 , io.IN , pull_up_down=io.PUD_UP)# GPIO4 as input
	rospy.init_node('safetyNode', anonymous=True)
	rospy.Subscriber("/cmd_vel_safe", Twist, callback)
	velocity_publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
	while not stop:
		safe = not io.input(7)
		if not safe:		
			vel_msg = Twist()
			vel_msg.linear.x = 0
			vel_msg.linear.y = 0
			vel_msg.linear.z = 0
			vel_msg.angular.x = 0
			vel_msg.angular.y = 0
			vel_msg.angular.z = 0
			velocity_publisher.publish(vel_msg)
			print('not safe')
		# check if the program should be stopped
		input = select.select([sys.stdin], [], [], 1)[0]
		if input:
			value = sys.stdin.readline().rstrip()
			if (value == "e"):
				stop = True
		time.sleep(0.01)